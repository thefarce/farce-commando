import a from '../index.js';



let sub  = a.subcommand(
  "sub",
  a.program(
    a.option('--test {String}'),
    $prog => $prog.getOption('test') || 'subby',
  )
);
let prog = a.program(
  sub,
  ($prog) => {
    console.log($prog);

    if ($prog.subcommand) {
      return $prog.subcommand.run();
    }
    return "proggy";
  }
);

let b;
prog("sub", "--test", "pork");
b = prog("sub", "friend", "--test", "pork");
//prog("--test", "pork", "sub");

console.log({b});
