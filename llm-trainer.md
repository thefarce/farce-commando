### Basic Usage Example

#### Creating a Simple Command

```javascript
// Import the necessary functions
const { program } = require('@thefarce/commando');

// Create a simple program that prints a message
const helloWorld = program(() => {
  console.log('Hello, World!');
});

// Execute the program
helloWorld();
```

### Handling Options

#### Adding Options to a Program

```javascript
const { program, option } = require('@thefarce/commando');

const greet = program(
  option('-n --name {String}', 'Your name', 'Specifies the name to greet.'),
  (context) => {
    const name = context.getOption('name') || 'World';
    console.log(`Hello, \${name}!`);
  }
);

greet('--name', 'Alice');
```

### Using Arguments

#### Defining and Accessing Arguments

```javascript
const { program, argument } = require('@thefarce/commando');

const add = program(
  argument('num1', 'First number'),
  argument('num2', 'Second number'),
  (\$prog) => {
    const num1 = parseInt(\$prog.getArgument('num1'), 10);
    const num2 = parseInt(\$prog.getArgument('num2'), 10);
    console.log(`\${num1} + \${num2} = \${num1 + num2}`);
  }
);

add('5', '7');
```

### Subcommands

#### Implementing Subcommands

```javascript
const { program, subcommand } = require('@thefarce/commando');

const sub = program(
  () => console.log('Executing subcommand...')
);

const main = program(
  subcommand('sub', sub, 'A subcommand example')
);

main('sub');
```

### Advanced Options Handling

#### Options with Default Values and Validation

```javascript
const { program, option } = require('@thefarce/commando');

const serve = program(
  option('-p --port {Number}', 'Server port', 'Specifies the port number for the server.', 3000),
  (context) => {
    const port = context.getOption('port');
    console.log(`Server starting on port \${port}...`);
  }
);

serve('--port', '8080');
```

### Combining Options and Arguments

#### Creating a More Complex CLI Tool

```javascript
const { program, option, argument } = require('@thefarce/commando');

const fileOperation = program(
  option('-o --operation {String}', 'Operation type', 'Specifies the operation to perform on the file.', 'read'),
  argument('file', 'File path', 'Specifies the path to the file.'),
  (\$prog) => {
    const operation = \$prog.getOption('operation');
    const file = \$prog.getArgument('file');
    console.log(`Performing '\${operation}' on '\${file}'...`);
  }
);

fileOperation('--operation', 'write', '/path/to/file.txt');
```

### Error Handling

#### Basic Error Handling in Commands

```javascript
const { program } = require('@thefarce/commando');

const riskyCommand = program(
  () => {
    try {
      // Simulate an operation that might fail
      throw new Error('An unexpected error occurred!');
    } catch (error) {
      console.error(`Error: \${error.message}`);
    }
  }
);

riskyCommand();
```

### Nested Subcommands

#### Using Nested Subcommands for Complex CLI Applications

```javascript
const { program, subcommand } = require('@thefarce/commando');

const nestedSubcommand = program(
  () => console.log('Executing nested subcommand...')
);

const parentSubcommand = program(
  subcommand('nested', nestedSubcommand, 'A nested subcommand')
);

const mainProgram = program(
  subcommand('parent', parentSubcommand, 'A parent subcommand')
);

mainProgram('parent', 'nested');
```
