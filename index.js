import argument   from './src/argument.js';
import option     from './src/option.js';
import program    from './src/program.js';
import subcommand from './src/subcommand.js';

export {
	argument,
  option,
  program,
  subcommand,
}

export default {
	argument,
  option,
  program,
  subcommand,
}
