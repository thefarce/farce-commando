* List subcommands in the help
* Hook into the help generation
* Add usage information
* Let the program's definition of --help override the default
* Optionally suppress the default help without providing your own (e.g. when
	you want to mount a 'help' subcommand).
* Default option/arg values from env



