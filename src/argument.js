/**
 * @module argument
 * @description This module provides a function for creating command line arguments.
 */

/**
 * Create a command line argument.
 * @param {string} name - The name of the argument.
 * @param {string} desc - The description of the argument.
 * @param {string} help - The help text for the argument.
 * @returns {object} - The argument object.
 * @throws {Error} - If the name is not provided or if the name is a program object.
 */
export default function argument (name, desc, help) {
  const result = {
    $: 'argument',
    spec: {
      name, desc, help
    }
  };

  // The second case here is when a name was omitted and a program given.
  if (!name || name.$ === 'program') {
    throw new Error('Each argument must have a name.');
  }

  return result;
}
