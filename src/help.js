/**
 * @module renderHelp
 */

/**
 * Render the help content for a command-line tool.
 * @param {Array} params - The parameters for the help content.
 * @param {string} style - The style of the help content.
 * @returns {string} - The rendered help content.
 */
export default function renderHelp (params, style) {
	let content = '';

	content += `MANUAL: ${style} (--help brief|manual|technical|full)\n\n`;

	/**
	 * Overview.
	 * Only include the overview section if there's content for it.
	 */
	let overview = params.filter(param => typeof param === "string");
	if (overview.length) {
		content += "OVERVIEW\n";
		content += params.filter(param => typeof param === "string").join('\n\n');
		content += '\n\n';
	}

	/**
	 * Options.
	 * There's always at least one option because of the --help option.
	 */
	let options = params.filter(param => param.$ === "option");

	content += "OPTIONS\n";
	params
		.filter(param => param.$ === "option")
		.map(param => {
			content += '    ' + param.tokens.sort().reverse().map(
				token => (token.length === 1 ? '-' : '--') + token
			).join(' ') +
				((param.isDeprecated) ? ' (DEPRECATED)' : '') + 
				` ${param.desc}\n`

			if (style === "brief") {
				return;
			}

			let chunks = ['This option'];

			chunks.push(`is${param.isRequired ? '' : ' not'} required`);
			chunks.push(`may ${param.allowMultiple || 'not'} be repeated`);

			let typesLine = `${param.takesArgument ? 'takes' : 'does not take'} an argument`;

			if (param.takesArgument) {
				typesLine += ` (${param.types.join(' | ').toLowerCase()})`;

				if (param.values?.length) {
					typesLine += ` that must be one of [${param.values.join(', ')}]`;
				}
			}

			chunks.push(typesLine);
			if (param.default) {
				chunks.push(`has a default value of "${param.default}"`);
			}

			let indent = '        ';
			if (["technical","full"].includes(style)) {
				switch (chunks.length) {
					/**
					 * By definition, we won't get 0-3 chunks.
						case 0:
						case 1:
							break;

						case 2:
							content += indent + chunks.join(' ') + '.';
							break;

						case 3:
							content += `${indent}${chunks.shift()} ${chunks.join(' and ')}.`;
							break;
							*/

					default:
					let first = chunks.shift();
					let last  = chunks.pop();
					content += `${indent}${first} ${chunks.join(', ')}, and ${last}.`;
					break;
				}
			}

			if (style === "full") {
				content += "\n\n";
			}

			if (param.help && ["manual","full"].includes(style)) {
				content += `${indent}${param.help}`;
			}

			content += "\n\n";
		});

	return content.trim();
}

