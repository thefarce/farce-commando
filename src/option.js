/**
 * @module option
 */

/**
 * Given a spec string, convert it into a spec object.
 *
 * @param {string} str - The spec string.
 * @returns {object} - The spec object.
 *
 * @example
 *
 * interpretSpecString('-f, --file <path> [default]')
 * // returns {
 * //   tokens: ['f', 'file'],
 * //   types: ['String'],
 * //   default: 'default',
 * //   flags: [],
 * //   isRequired: false,
 * //   allowMultiple: false,
 * //   isDeprecated: false
 * // }
 */
function interpretSpecString (str) {
  str = str.trim();
  const spec = {
    tokens: [],
  };

  str = str.trim();

  /**
   * These are a series of successive reductions of the input string.  It
   * starts with, for example:
   *
   *    <tokens> <type> <values> <description>
   *
   * First, the tokens are parsed.  The string is modified in place and
   * becomes:
   *
   *    <type> <values> <description>
   *  
   * And so on.
   */

  // Handle tokens
  const tokenPattern = /-{1,2}[a-zA-Z0-9-?]+/;
  let tokenMatch;
  while (tokenMatch = str.match(tokenPattern)) {
    spec.tokens.push(tokenMatch[0].replace(/^-{1,2}/, ''));
    str = str.replace(tokenPattern, '');
  }

  // Handle types
  const typePattern = /{(?:(String|Str|Boolean|Bool|Number|Num|Integer|Int)\|?)+}/;
  const typeMatch = str.match(typePattern);
  if (typeMatch) {
    spec.types = typeMatch[0]
      .replace(/[{}]/g, '')
      .split('|')
      .map((type) => type.trim());
    str = str.replace(typePattern, '');
  }

  const badTypePattern = /{[^)]+}/;
  const badTypeMatch = str.match(badTypePattern);
  if (badTypeMatch) {
    throw(`Unrecognized type in {${badTypeMatch[1]}}`);
  }


  /**
   * Handle enumerated values
   * (a|b|c)
   *
   * The previous version of this regexp took around 100 times longer to run
   * than the current version.  The two are:
   *    slower:  valuesPattern = /\((?:(.+)\|?)+\)/;
   *    faster:  valuesPattern = /\([^)]+|[^)]+\)/;
   */
  const valuesPattern = /\([^)]+\|[^)]+\)/;
  const valuesMatch = str.match(valuesPattern);

  if (valuesMatch) {
    spec.values = valuesMatch[0]
      .replace(/[\(\)]/g, '')
      .split('|')
      .map((type) => type.trim());
    str = str.replace(valuesPattern, '');
  }

  // Handle internal representation
  /*
  const internalPattern = /<([a-zA-Z0-9]+)>/;
  const internalMatch = str.match(internalPattern);
  if (internalMatch) {
    spec.internal = internalMatch[1];
    str = str.replace(internalPattern, '');
  }
  */

  // Handle default
  const defaultPattern = /\[([a-zA-Z0-9]+)\]/;
  const defaultMatch = str.match(defaultPattern);
  if (defaultMatch) {
    spec.default = defaultMatch[1];
    str = str.replace(defaultPattern, '');
  }
  
  /**
   * Anything left is interpreted as flags.
   */
  spec.flags = str.trim()
    .replace(/\s/g, '')
    .split('')
    .filter(a => a.trim())
  ;

  spec.isRequired    = !!spec.flags.includes('!');
  spec.allowMultiple = !!spec.flags.includes('@');
  spec.isDeprecated  = !!spec.flags.includes('~');

  return spec;
}

/**
 * Create an option object.
 *
 * @param {string|object} spec - The option specification.
 * @param {string} [desc=""] - The option description.
 * @param {string} [help=""] - The option help text.
 * @returns {object} - The option object.
 *
 * @example
 *
 * option('-f, --file <path> [default]', 'Specify a file', 'This option allows you to specify a file.')
 * // returns {
 * //   $: 'option',
 * //   desc: 'Specify a file',
 * //   help: 'This option allows you to specify a file.',
 * //   spec: {
 * //     tokens: ['f', 'file'],
 * //     types: ['String'],
 * //     default: 'default',
 * //     flags: [],
 * //     isRequired: false,
 * //     allowMultiple: false,
 * //     isDeprecated: false
 * //   },
 * //   isRequired: false,
 * //   allowMultiple: false,
 * //   isDeprecated: false,
 * //   takesArgument: true,
 * //   rendered: '-f, --file <path> [default]'
 * // }
 */
export default function option (
  spec,
  desc="",
  help=""
) {
  // The special '$' key is for internal tracking.  It lets the process
  // identify this as an option specification.
  const result = { $: 'option', desc, help };

  if (typeof spec === 'string') {
    spec = interpretSpecString(spec);
  }

  result.spec = spec;

  Object.entries({
    isDeprecated  : validateIsDeprecated,
    allowMultiple : validateAllowMultiple,
    default       : validateDefault,
    description   : validateDescription,
    //internal      : validateInternal,
    isRequired    : validateIsRequired,
    tokens        : validateTokens,
    types         : validateTypes,
    values        : validateValues,
  }).forEach(([field, validator]) => {
    if (typeof spec[field] !== 'undefined') {
      result[field] = JSON.parse(JSON.stringify(spec[field]));
    }
  });

  // This set of validations is a sieve.  It will throw on any invalid
  // parameter or combinations of parameter.  It relies on "result" having
  // been already processed.
  //
  // For ease of user experience, we want to throw in something like order
  // of importance.  For example, not having any tokens is much more severe
  // of a problem than not having a description.
  //
  validateTokens(result);
  validateAllowMultiple(result);
  validateDefault(result);
  validateDescription(result);
  //validateInternal(result);
  validateIsRequired(result);
  validateIsDeprecated(result);
  validateTypes(result);
  validateValues(result);

  // An option does not take an argument _only_ when it is Boolean.
  result.takesArgument = !(
    (result.types || []).includes('Boolean')
    || (result.types || []).includes('Bool')
    || (result.types || []).length === 0
  );

  // Also provide a rendered version of the string.  This is the string
  // version of its specification.
  result.rendered = renderString(result);

  return result;
}

/**
 * Validate the allowMultiple field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the allowMultiple field is valid, false otherwise.
 * @throws {Error} - If the allowMultiple field is not a boolean.
 */
function validateAllowMultiple (spec) {
  if (
    typeof spec.allowMultiple !== 'undefined'
    && spec.allowMultiple !== true
    && spec.allowMultiple !== false
  ) {
    throw new Error(`allowMultiple must be a boolean; got: ${spec.allowMultiple}`);
  }

  return true;
}

/**
 * Validate the default field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the default field is valid, false otherwise.
 * @throws {Error} - If the default field is not a valid type.
 */
function validateDefault (spec) {
  // Allow undefined.
  if (typeof spec.default === 'undefined') {
    return true;
  }

  if (
    Array.isArray(spec.default)
  ) {
    throw new Error(
      'Option defaults must be of a valid type; '
      + `received:  ${JSON.stringify(spec.default)}`,
    );
  }

  return true;
}

/**
 * Validate the description field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the description field is valid, false otherwise.
 * @throws {Error} - If the description field is not a string.
 */
function validateDescription (spec) {
  // Allow undefined.
  if (typeof spec.description === 'undefined') {
    return true;
  }

  if (typeof spec.description !== 'string') {
    throw new Error(
      `Option descriptions must be strings:  ${spec.description}`,
    );
  }

  return true;
}

/**
 * Validate the isRequired field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the isRequired field is valid, false otherwise.
 * @throws {Error} - If the isRequired field is not a boolean.
 */
function validateIsRequired (spec) {
  if (
    typeof spec.isRequired !== 'undefined'
    && spec.isRequired !== true
    && spec.isRequired !== false
  ) {
    throw new Error(`isRequired must be a boolean; got: ${spec.isRequired}`);
  }

  return true;
}

/**
 * Validate the isDeprecated field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the isDeprecated field is valid, false otherwise.
 * @throws {Error} - If the isDeprecated field is not a boolean.
 */
function validateIsDeprecated (spec) {
  if (
    typeof spec.isDeprecated !== 'undefined'
    && spec.isDeprecated !== true
    && spec.isDeprecated !== false
  ) {
    throw new Error(`isDeprecated must be a boolean; got: ${spec.isDeprecated}`);
  }

  return true;
}

/**
 * Validate the tokens field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the tokens field is valid, false otherwise.
 * @throws {Error} - If the tokens field is not an array or contains invalid values.
 */
function validateTokens (spec) {
  // Allow undefined.
  if (typeof spec.tokens === 'undefined') {
    throw new Error('Option tokens must be specified.');
  }

  if (!Array.isArray(spec.tokens)) {
    throw new Error('Option tokens must be an array.');
  }

  if (spec.tokens.length == 0) {
    throw new Error('Option tokens cannot be an empty array.');
  }

  spec.tokens.forEach((token) => {
    if (typeof token !== 'string') {
      throw new Error(
        `Tokens must be strings; received:  ${JSON.stringify(token)}`,
      );
    }

    if (!token.length) {
      throw new Error('Tokens cannot be empty strings.');
    }

    if (token.length > 1 && token.match(/\?/)) {
      throw new Error(`Long options cannot contain question marks:  ${token}`);
    }
  });

  return true;
}

/**
 * Validate the types field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the types field is valid, false otherwise.
 * @throws {Error} - If the types field is not an array or contains invalid values.
 */
const TYPES = [
  'String', 'Str',
  'Number', 'Num',
  'Integer', 'Int',
  'Boolean', 'Bool',
];

function validateTypes (spec) {
  // Allow undefined.
  if (typeof spec.types === 'undefined') {
    return true;
  }

  if (!Array.isArray(spec.types)) {
    throw new Error('Option types must be an array.');
  }

  if (spec.types.length == 0) {
    throw new Error('Option types cannot be an empty array.');
  }

  spec.types.forEach((type) => {
    if (!TYPES.includes(type)) {
      throw new Error([
        `Invalid option type specified:  ${type}`,
        `Option types must be one of:  ${TYPES.join(', ')}`,
      ].join('\n'));
    }
  });

  return true;
}

/**
 * Validate the values field of the spec object.
 *
 * @param {object} spec - The option spec object.
 * @returns {boolean} - True if the values field is valid, false otherwise.
 * @throws {Error} - If the values field is not an array or contains invalid values.
 */
function validateValues (spec) {
  // Allow undefined.
  if (typeof spec.values === 'undefined') {
    return true;
  }

  if (!Array.isArray(spec.values)) {
    throw new Error('Option values must be an array.');
  }

  if (spec.values.length == 0) {
    throw new Error('Option values cannot be an empty array.');
  }

  return true;
}

/**
 * Renders a symbol indicating whether multiple items are allowed.
 *
 * @param {Object} spec - The specification object.
 * @param {boolean} spec.allowMultiple - Indicates whether multiple items are allowed.
 * @returns {string} - The rendered symbol.
 *
 * @example
 * const spec = {
 *   allowMultiple: true
 * };
 * const result = renderAllowMultiple(spec);
 * console.log(result); // Output: '+'
 */
function renderAllowMultiple (spec) {
  let output = '';
  if (spec.allowMultiple === true) {
    output += '+';
  }
  return output;
}

/**
 * Render the default value of an option spec.
 * @param {Object} spec - The option spec.
 * @returns {string} - The rendered default value.
 */
function renderDefault (spec) {
  let output = '';
  if (typeof spec.default !== 'undefined') {
    output += `[${spec.default}]`;
  }
  return output;
}

/**
 * Render the "isRequired" indicator of an option spec.
 * @param {Object} spec - The option spec.
 * @returns {string} - The rendered "isRequired" indicator.
 */
function renderIsRequired (spec) {
  let output = '';
  if (spec.isRequired === true) {
    output += '!';
  }
  return output;
}

/**
 * Render the tokens of an option spec.
 * @param {Object} spec - The option spec.
 * @returns {string} - The rendered tokens.
 */
function renderTokens (spec) {
  const tokenization = [];
  spec.tokens.forEach((token) => {
    if (token.length === 1) {
      tokenization.push(`-${token}`);
    } else {
      tokenization.push(`--${token}`);
    }
  });
  return tokenization.join(' ');
}

/**
 * Render the types of an option spec.
 * @param {Object} spec - The option spec.
 * @returns {string} - The rendered types.
 */
function renderTypes (spec) {
  const typeList = [];

  if (Array.isArray(spec.types)) {
    spec.types.forEach((type) => {
      typeList.push(type);
    });
  }

  if (typeList.length) {
    return `{${typeList.join('|')}}`;
  }

  return '';
}

/**
 * Render the values of an option spec.
 * @param {Object} spec - The option spec.
 * @returns {string} - The rendered values.
 */
function renderValues (spec) {
  const valueList = [];

  if (Array.isArray(spec.values)) {
    spec.values.forEach((value) => {
      valueList.push(value);
    });
  }

  if (valueList.length) {
    return `(${valueList.join('|')})`;
  }

  return '';
}

/**
 * Render the option spec as a string.
 * @param {Object} spec - The option spec.
 * @returns {string} - The rendered option spec.
 */
function renderString (spec) {
  return [
    renderIsRequired(spec) + renderAllowMultiple(spec),
    renderTokens(spec),
    renderTypes(spec),
    //renderInternal(spec),
    renderValues(spec),
    renderDefault(spec),
    //renderDescription(spec),
  ]
    .filter((val) => val.length)
    .join(' ').trim();
}
