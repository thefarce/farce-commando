
# Options

The entry point for options is the `option` function.  The `option` function
takes a single _specification_ argument that describes the option.

The specification argument can be either a string or a Parameterized
Specification Object (PSO).  Specification strings are parsed and converted into
parameterized specification objects internally.  PSOs provide
string-rendered versions of themselves as well.  The two formats are
designed to be completely interchangable.

The following attributes may be specified.

### allowMultiple

**Default:  false**

Whether an invocation of the script will permit multiple occurrences of this
option.  For example, an option `+ --input {String}` allows multiple
`--input` options to be specified, like this:  `myprog --input a --input b`.
These will be represented internally as an array:  `['a','b']`

To specify in SPO format, use the parameter `allowMultiple`.  The value must
be either `true` or `false` if provided.  It may be omitted.

To specify in String format, prefix the option string with a `+` sign.

#### Examples

SPO Format
```
{
  allowMultiple: true,
  ...
}
```

String formats
```
// equivalent to {allowMultiple: true}
+ -i --input {String}
// equivalent to {allowMultiple: false}
-i --input {String}
```

### default

Specifies the default value for the option.

#### Object format

Use the `default` parameter to set the option's default value.

#### String format
#### Examples
SPO Format
```
{
  default: "blue",
  ...
}
```
String formats
```
-c --color {String} [blue]
```

### description

Provide a brief description of the option.  This can be used to provide help
text or similar outputs.

#### Object format
#### String format
#### Examples
SPO Format
```
{
  description: "The color to use",
  ...
}
```
String formats
```
-c --color {String} The color to use
```

### internal
#### Object format
#### String format
#### Examples
SPO Format
```
{
  internal: 'useColor',
  ...
}
```
String formats
```
-c --use-color {String} <useColor>
```

### isRequired

**Default: false**

Indicate that an option is required.

To specify in SPO format, use the parameter `isRequired`.  The value must
be either `true` or `false` if provided.  It may be omitted.

To specify in String format, prefix the option string with a `!` sign.

#### Examples
SPO Format
```
{
  isRequired: true,
  ...
}
```
String formats
```
! -c --color {String}
```

### tokens

Each option has one or more _tokens_.  Each token is can be used to
determine the value's option.  For example, consider an option specified
with the following string:

```
-c --color --use-color {String}
```

During runtime, this option can be accessed using any of the provided
tokens:

```
// invoked with:  mycommand --use-color red
($prog) => {
  // These three all return "red"
  let color;
  color = $prog.getOption('c');
  color = $prog.getOption('color');
  color = $prog.getOption('use-color');
}
```

#### Examples
SPO Format
```
{
  tokens: ['c','colors','use-color'],
  ...
}
```
String formats
```
-c --color --use-color {String}
```

### types

Commando options let you hint at argument type.

#### Examples
SPO Format
```
```
String formats
```
```

### values
#### Object format
#### String format
#### Examples
SPO Format
```
```
String formats
```
```

### 
#### Object format
#### String format
#### Examples
SPO Format
```
```
String formats
```
```




/**
 * Given a spec string, convert it into a spec object.
 */
function interpretSpecString (str) {
  str = str.trim();
  let spec = {
    tokens: [],
  };

  while (str[0] === '!' || str[0] === '+') {
    switch (str[0]) {
      case '!':
        spec.isRequired = true;
        break;
      case '+':
        spec.allowMultiple = true;
        break;
    }
    str = str.substr(1);
  }

  str = str.trim();

  // Handle types
  let typePattern = /{(?:(String|Str|Boolean|Bool|Number|Num|Integer|Int)\|?)+}/;
  let typeMatch = str.match(typePattern);
  if (typeMatch) {
    spec.types = typeMatch[0]
      .replace(/[{}]/g,'')
      .split('|')
      .map(type => type.trim());
    str = str.replace(typePattern, '');
  } else {
    throw new Error("Argument type must be one of "
      + "String Str Boolean Bool Number Num Integer Int "
      + "wrapped in set braces.  For example:  {String}"
    );
  }


  // Handle enumerated values
  let valuesPattern = /\((?:(.+)\|?)+\)/;
  let valuesMatch = str.match(valuesPattern);
  if (valuesMatch) {
    spec.values = valuesMatch[0]
      .replace(/[\(\)]/g,'')
      .split('|')
      .map(type => type.trim());
    str = str.replace(valuesPattern, '');
  }

  // Handle tokens 
  let tokenPattern = /-{1,2}[a-zA-Z0-9-?]+/;
  let tokenMatch;
  while (tokenMatch = str.match(tokenPattern)) {
    spec.tokens.push(tokenMatch[0].replace(/^-{1,2}/,''));
    str = str.replace(tokenPattern, '');
  }

  // Handle internal representation
  let internalPattern = /<([a-zA-Z0-9]+)>/;
  let internalMatch = str.match(internalPattern);
  if (internalMatch) {
    spec.internal = internalMatch[1];
    str = str.replace(internalPattern, '');
  }

  // Handle default 
  let defaultPattern = /\[([a-zA-Z0-9]+)\]/;
  let defaultMatch = str.match(defaultPattern);
  if (defaultMatch) {
    spec.default = defaultMatch[1];
    str = str.replace(defaultPattern, '');
  }

  spec.description = str.trim();

  return spec;
}

export default function option (spec) {
  // The special '$' key is for internal tracking.  It lets the process
  // identify this as an option specification.
  const result = {'$': 'option'};

  if (typeof spec === 'string') {
    spec = interpretSpecString(spec);
  }

  Object.entries({
    allowMultiple : validateAllowMultiple,
    default       : validateDefault,
    description   : validateDescription,
    internal      : validateInternal,
    isRequired    : validateIsRequired,
    tokens        : validateTokens,
    types         : validateTypes,
    values        : validateValues,
  }).forEach(([field,validator]) => {
    if (typeof spec[field] !== 'undefined') {
      result[field] = JSON.parse(JSON.stringify(spec[field]));
    }
  });

  // This set of validations is a sieve.  It will throw on any invalid
  // parameter or combinations of parameter.  It relies on "result" having
  // been already processed.
  validateAllowMultiple(result);
  validateDefault(result);
  validateDescription(result);
  validateInternal(result);
  validateIsRequired(result);
  validateTokens(result);
  validateTypes(result);
  validateValues(result);

  // An option does not take an argument _only_ when it is Boolean.
  result.takesArgument = !(
    (result.types || []).includes('Boolean') ||
    (result.types || []).includes('Bool')    ||
    (result.types || []).length === 0
  );

  // Also provide a rendered version of the string.  This is the string
  // version of its specification.
  result.rendered = renderString(result);

  return result;
};

function validateAllowMultiple (spec) {
  if (
    typeof spec.allowMultiple !== 'undefined' &&
    spec.allowMultiple        !== true        &&
    spec.allowMultiple        !== false
  ) {
    throw new Error("allowMultiple must be a boolean; got: " + spec.allowMultiple);
  }

  return true;
}

function validateDefault (spec) {
  // Allow undefined.
  if (typeof spec.default === 'undefined') {
    return true;
  }

  if (
    Array.isArray(spec.default)
  ) {
    throw new Error(
      "Option defaults must be of a valid type; " +
      `received:  ${JSON.stringify(spec.default)}`
    );
  }

  return true;
}

function validateDescription (spec) {
  // Allow undefined.
  if (typeof spec.description === 'undefined') {
    return true;
  }

  if (typeof spec.description !== 'string') {
    throw new Error(
      `Option descriptions must be strings:  ${spec.description}`
    );
  }

  return true;
}

function validateInternal (spec) {
  // Allow undefined.
  if (typeof spec.internal === 'undefined') {
    return true;
  }

  if (
    typeof spec.internal !== 'string' ||
    !spec.internal.match(/^[a-zA-Z0-9]+$/)
  ) {
    throw new Error(
      `Option internal names must be alphanumeric strings:  ${spec.internal}`
    );
  }

  return true;
}

function validateIsRequired (spec) {
  if (
    typeof spec.isRequired !== 'undefined' &&
    spec.isRequired !== true &&
    spec.isRequired !== false
  ) {
    throw new Error("isRequired must be a boolean; got: " + spec.isRequired);
  }

  return true;
}

function validateTokens (spec) {
  // Allow undefined.
  if (typeof spec.tokens === 'undefined') {
    throw new Error('Option tokens must be specified.');
  }

  if (!Array.isArray(spec.tokens)) {
    throw new Error('Option tokens must be an array.');
  }

  if (spec.tokens.length == 0) {
    throw new Error('Option tokens cannot be an empty array.');
  }

  spec.tokens.forEach(token => {
    if (typeof token !== "string") {
      throw new Error(
        `Tokens must be strings; received:  ${JSON.stringify(token)}`
      );
    }

    if (!token.length) {
      throw new Error("Tokens cannot be empty strings.");
    }

    if (token.length > 1 && token.match(/\?/)) {
      throw new Error(`Long options cannot contain question marks:  ${token}`);
    }
  });

  return true;
}

const TYPES = [
  'String'  , 'Str'  ,
  'Number'  , 'Num'  ,
  'Integer' , 'Int'  ,
  'Boolean' , 'Bool' ,
];

function validateTypes (spec) {
  // Allow undefined.
  if (typeof spec.types === 'undefined') {
    return true;
  }

  if (!Array.isArray(spec.types)) {
    throw new Error('Option types must be an array.');
  }

  if (spec.types.length == 0) {
    throw new Error('Option types cannot be an empty array.');
  }

  spec.types.forEach(type => {
    if (!TYPES.includes(type)) {
      throw new Error([
        `Invalid option type specified:  ${type}`,
        `Option types must be one of:  ${TYPES.join(', ')}`
      ].join('\n'));
    }
  });

  return true;
}

function validateValues (spec) {
  // Allow undefined.
  if (typeof spec.values === 'undefined') {
    return true;
  }

  if (!Array.isArray(spec.values)) {
    throw new Error('Option values must be an array.');
  }

  if (spec.values.length == 0) {
    throw new Error('Option values cannot be an empty array.');
  }

  return true;
}


function renderAllowMultiple (spec) {
  let output = '';
  if (spec.allowMultiple === true) {
    output += '+';
  }
  return output;
}

function renderDefault (spec) {
  let output = '';
  if (typeof spec.default !== "undefined") {
    output += `[${spec.default}]`;
  }
  return output;
}

function renderDescription (spec) {
  let output = '';
  if (typeof spec.description !== "undefined") {
    output += spec.description;
  }
  return output;
}

function renderInternal (spec) {
  let output = '';
  if (typeof spec.internal !== "undefined") {
    output += `<${spec.internal}>`;
  }
  return output;
}

function renderIsRequired (spec) {
  let output = '';
  if (spec.isRequired === true) {
    output += '!';
  }
  return output;
}

function renderTokens (spec) {
  let tokenization = [];
  spec.tokens.forEach(token => {
    if (token.length === 1) {
      tokenization.push('-' + token);
    } else {
      tokenization.push('--' + token);
    }

  });
  return tokenization.join(' ');
}

function renderTypes (spec) {
  let typeList = [];

  if (Array.isArray(spec.types)) {
    spec.types.forEach(type => {
      typeList.push(type);
    });
  }

  if (typeList.length) {
    return '{' + typeList.join('|') + '}';
  }

  return '';
}

function renderValues (spec) {
  let valueList = [];

  if (Array.isArray(spec.values)) {
    spec.values.forEach(value => {
      valueList.push(value)
    });
  }

  if (valueList.length) { 
    return '(' + valueList .join('|') + ')';
  }

  return '';
}

/** Given an option string, render its spec.
*/
/*
allowMultiple
default
description
internal
isRequired
tokens
types
values
*/

function renderString (spec) {
  return [
    renderIsRequired(spec) + renderAllowMultiple(spec),
    renderTokens(spec),
    renderTypes(spec),
    renderInternal(spec),
    renderValues(spec),
    renderDefault(spec),
    renderDescription(spec),
  ]
    .filter(val => val.length)
    .join(' ').trim();
}
