/**
 * @module subcommand
 */

/**
 * Creates a subcommand with the given name and program.
 *
 * @param {string} name - The name of the subcommand.
 * @param {Program} prog - The program to run for the subcommand.
 * @returns {Program} - The subcommand program.
 * @throws {Error} - If the name or program is missing, or if the program is not created with program().
 */
export default function subcommand(name, prog) {
  if (!name || name.$ === 'program') {
    throw new Error('Each subcommand must have a name.');
  }

  if (!prog) {
    throw new Error('Each subcommand must have a program to run.');
  }

  if (prog.$ !== 'program') {
    throw new Error("A subcommand's program must be created with program().");
  }

  prog.$name = name;
  prog.$ = 'subcommand';

  return prog;
}
