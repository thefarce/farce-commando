/**
 * This module provides a function for creating command line programs.
 * The `program` function takes a series of arguments that define the
 * program's options, subcommands, and executable function. It returns
 * a function that can be called to run the program.
 *
 * @module program
 */
import option from './option.js';
import renderHelp from './help.js';

/**
 * Create a new command line program.
 *
 * @param {...(option|subcommand|function|string)} params - The program
 *   definition. This can include options, subcommands, an executable
 *   function, and a summary string.
 *
 * @returns {function} - A function that can be called to run the program.
 */
export default function program (...params) {
  const spec = {
    executable  : null,
    options     : [],
    arguments   : [],
    subcommands : [],
  };

  params.unshift(option(
    '-h -? --help {String} (brief|manual|technical|full) [brief]',
    'Displays help content',
    'This option creates programmatically-formatted help for the program.'
  ));

  params.forEach((param) => {
    if (typeof param === 'function' && param.$ !== 'subcommand') {
      if (typeof spec.executable === 'function') {
        throw new Error('Programs cannot have multiple handler functions.');
      }
      spec.executable = param;
    }

    else if (param.$ === 'option') {
      spec.options.push(param);
    }

    else if (param.$ === 'subcommand') {
      spec.subcommands.push(param);
    }

    else if (param.$ === "argument") {
      spec.arguments.push(param);
    }

    else if (typeof param === "string") {
      spec.summary = param;
    }

    else {
      throw new Error(
        'Program arguments must be options, subcommands, or functions.',
      );
    }
  });

  if (!spec.executable) {
    spec.executable = () => {};
  }

  /**
   * The return value of the program() is a function.  This function does a
   * couple of things.
   *
   *    First, each time you run the program, it creates a new runtime by
   *    comparing the arguments to spec.  In this context, a "runtime" is
   *    defined as the arguments to the program applied to the program's
   *    definition (spec).
   *
   *    Second, it creates a method called .getOption(token).  This method
   *    lets you access options by token in a program's executable.
   */
  const prog = function (...args) {
    /**
     * This allows us to call subcommands with implicit args (taken from the
     * command line).
     */
    if (!args.length && this?.args?.length) {
      args = this.args;
    }

    const runtime = interpret(args, spec);

    /**
     * @param token - Any of the tokens specified for an option
     * @returns The value of the associated token if available, otherwise
     *          return undefined.
     */
    runtime.getOption = (token) => {
      // Look through the provided options looking for the requested token.
      for (let o = 0; o < runtime.options.length; o++) {
        for (let t = 0; t < runtime.options[o].spec.tokens.length; t++) {
          // If we find a match, return the value.
          if (runtime.options[o].spec.tokens[t] === token) {
            return runtime.options[o].value;
          }
        }
      }

      // We couldn't find the requested token in the current argument list,
      // so return undefined.
      return undefined;
    };

    /**
     * Get an argument by name
     */
    runtime.getArgument = (token) => {
      for (let a = 0; a < runtime.arguments.length; a++) {
        let arg = runtime.arguments.filter(arg => arg.spec.name === token);

        return arg[0].value;
      }
    }

    if (runtime.getOption('help')) {
      process.stderr.write(renderHelp(params, runtime.getOption('help'))+"\n\n");
      return;
    }

    return spec.executable(runtime);
  };

  prog.$ = 'program';
  prog.spec = spec;

  return prog;
}

/**
 * Given an argument array (args), and a program specification, interpret
 * all the arguments for the runtime.
 *
 * @param {Array} args - An array of arguments as they would be passed in through process.argv during invocation.
 * @param {Object} prog - The program specification.
 * @returns {Object} - The interpreted runtime.
 */
function interpret (args, prog) {
  const runtime = {
    options    : [],
    arguments  : [],
    subcommand : undefined,
  };

  for (let a = 0; a < args.length; a++) {
    if (isOptFlag(args[a])) {
      const [opt, token] = findOption(args[a], prog.options);

      if (!opt) {
        throw new Error(`Unknown option:  ${args[a]}`);
      }

      if (!opt.takesArgument) {
        runtime.options.push({
          position : a,
          spec     : opt,
          token,
          value    : true,
        });
        continue;
      }

      if (a + 1 === args.length) {
        if (opt.default !== undefined) {
          args.splice(a+1, 0, opt.default);
        } else {
          throw new Error(`Required argument not provided for option:  ${args[a]}`);
        }
      }

      runtime.options.push({
        position : a,
        spec     : opt,
        token,
        value    : args[a + 1],
      });

      a += 1;
      continue;
    }

    const subcmd = getSubcommand(args[a], prog.subcommands);
    if (subcmd) {
      runtime.subcommand = {
        mount    : args[a],
        run      : subcmd,
        args     : args.slice(a + 1),
        position : a,
      };
      break;
    }

    let _args = JSON.parse(JSON.stringify(args));

    prog.arguments.forEach((argument, a) => {
      runtime.arguments.push({
        position: a,
        value: _args.shift(),
        spec: argument.spec,
      });
    });

    _args.forEach((arg) => {
      runtime.arguments.push({
        position: runtime.arguments.length,
        value: arg,
        spec: {},
      });
    });
  }

  return runtime;
}

/**
 * Check whether this argument is a specified subcommand.  If so, return
 * that subcommand.  If not, return null.
 *
 * @param {string} arg - The argument to check.
 * @param {Array} subcommands - The list of subcommands to check against.
 * @returns {Object|null} - The matching subcommand or null if no match is found.
 */
function getSubcommand(arg, subcommands) {
  return subcommands.find((subcmd) => subcmd.$name === arg) || null;
}

/**
 * Check whether this argument is an option flag.
 *
 * @param {string} arg - The argument to check.
 * @returns {boolean} - True if the argument is an option flag, false otherwise.
 */
function isOptFlag(arg) {
  return typeof arg === 'string' && arg[0] === '-';
}

/**
 * Given a flag and an option list, return the option that matches it (if
 * any), and the token of the flag.
 *
 * @param {string} flag - The flag to match.
 * @param {Array} options - The list of options to search.
 * @returns {Array} - An array containing the matching option and the token of the flag.
 */

function findOption(flag, options) {
  const flagToken = flag.replace(/^-+/g, '');
  const option = options.find((opt) =>
    opt.tokens.some((token) => token === flagToken)
  );
  return [option, flagToken];
}

