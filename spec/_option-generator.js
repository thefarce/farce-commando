/**
 * Options.
 *
 * There are lots of ways to build options.  Because there are so many,
 * there are even more total possible combinations.  This attempts to be a
 * pretty robust catalog of possible options combinations to test them
 * thoroughly.
 *
 * The big deal here is to define each kind of parameter, then create our
 * many combinations.
 *
 *
 *
 */

let parameters = [];

/**
 * isRequired - Boolean - Can occur zero or one time.
 * Whether or not the option is required.
 * Represented by an exclaimation point in the "flags" section of the option
 * string.
 * Example:  ! ...
 */
parameters.push(['', '~', '!', '+', '!+', '@!?']);

/**
 * allowMultiple
 * Boolean.
 * Can occur zero or one time.
 * Can this option occur multiple times.
 * Represented by a plus in the "flags" section of the option string.
 * Example:  + ...
 */

/**
 * short-token
 * String (starts with a single hyphen).
 * Can appear any number of times.
 * A one-character token for this option.
 * Represented as a single character prefixed by a single hyphen.
 * Example:  -c
 */
parameters.push(['', '-c', '-c -r']);

/**
 * long-token
 * String (starts with a double hyphen).
 * Can appear multiple times.
 * A multi-character token for this option.
 * Represented as multiple characters prefixed by a double hyphen.
 * Example:  --color
 */
parameters.push(['', '--color', '--color --set-color']);

/**
 * type
 * (String|Int|Number|Boolean|...)
 * Can have multiple values (pipe-delimited).
 * A string indicating the type the option's value should take.
 * Represented by a valid type enclosed in set braces.
 * Example:  {String|Int}
 */
parameters.push(['', '{String}', "{Integer}", "{Number}", "{Boolean}"]);

/**
 * options
 * List.
 * Can have zero or one occurrence.
 * A string of pipe-delimited values enclosed in parentheses.
 * Example:  (red|blue|green)
 */
parameters.push(['', '(red|blue)']);

/**
 * default
 * Any
 * Can have zero or one occurrence.
 * A default value if the option is omitted.
 * Represented by pipe-delimited strings enclosed in square brackets.
 */
parameters.push(['', '[red]']);


/**
 * description
 * Text
 * Can have one occurrence.
 * A description of the option.
 * Arbitrary Markdown content describing the option's use and
 * interpretation.
 */

function cartesian(args) {
	var r = [], max = args.length-1;
	function helper(arr, i) {
		for (var j=0, l=args[i].length; j<l; j++) {
			var a = arr.slice(0); // clone arr
			a.push(args[i][j]);
			if (i==max)
				r.push(a);
			else
				helper(a, i+1);
		}
	}
	helper([], 0);
	return r;
}

const combinations = cartesian(parameters);

/**
 * This function simulates the logic of the spec in our constrained param
 * space.
 */
function labelParams (params) {
  let result = {
    isDeprecated  : params[0].match('~') ? true : false,
    isRequired    : params[0].match('!') ? true : false,
    allowMultiple : params[0].match('@') ? true : false,
    tokens        : [
      ...params[1].trim().split(' '),
      ...params[2].trim().split(' '),
    ],
    types: params[3].trim().replace(/^{(.*)}$/, "$1").split('|'),
    values: params[4].trim()
    .replace(/^\((.*)\)$/, "$1")
    .split('|')
    .filter(a => a),
    default       : params[5].trim().replace(/^\[(.*)\]$/, "$1") || undefined,
  };

  if (result.values.length === 0) {
    delete result.values;
  }

  result.tokens = result.tokens.filter(a => a).map(a => a.replace(/^-+/,''));
  result.tokens = result.tokens.length ? result.tokens : undefined;

  result.types = result.types.filter(a => a);
  result.types = result.types.length ? result.types : undefined;

  return result;
}

function createTest (optionStr, params) {
  if (!params.tokens || !params.tokens.length) {
    return `
    // ${params}
    test('${optionStr}', () => {
      expect(() => option("${optionStr}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    `;
  }

  return `
    test('${optionStr}', () => {
      expect(option("${optionStr}").spec)
        .toMatchObject(${
          JSON.stringify(params,undefined,2)
            .replace(/\n/gm, '\n        ')
        });
    });
  `
}

combinations.forEach((combo,count) => {
  // Limit to the first 25.
  if (count <= 20) {
    return;
  }

  let optionStr = combo.join(' ').trim()
    .replace(/\s+/g, ' ')
    .replace('! +', '!+');

  console.log(createTest(optionStr, labelParams(combo)));
});

