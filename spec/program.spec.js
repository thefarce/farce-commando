import info       from '../package.json'
import option     from '../src/option.js'
import program    from '../src/program.js'
import subcommand from '../src/subcommand.js'
import suite      from './suite-generator.js'
import argument   from '../src/argument.js';

suite([info.version, 'Programs'], () => {
	/**
	 * Happy path describes tests that describe the program's execution when
	 * everything is used as expected and things work well.
	 *
	 * In HTTP terms, these are like 200-series statuses.
	 */
	describe("happy-path", () => {

		test('should set the summary property when a string parameter is provided', () => {
			const summary = 'This is a test program';
			const myProgram = program(summary);
			expect(myProgram.spec.summary).toEqual(summary);
		});

		test('should set the summary property when a string parameter is provided', () => {
			expect(() => program([])).toThrow("Program arguments must be options, subcommands, or functions.");
		});

		test("creation", () => {
			const prog = program();
			expect(typeof prog).toBe('function');
		});

		test("with an endpoint", () => {
			const prog = program(() => 44);
			expect(prog()).toBe(44);
		});

		test("without an endpoint", () => {
			const prog = program();
			expect(prog()).toBe();
		});

		test("with options", () => {
			// Take no flag value
			let prog = program(
				option({tokens:['a']}),
				(context) => context.getOption('a')
			);
			expect(prog('-a', 35)).toBe(true)

			// Take a string value
			prog = program(
				option('-a --apple {String}'),
				(context) => context.getOption('apple'),
			);
			expect(prog('--apple', 'pie')).toBe('pie');
			expect(prog('-a', 'fritter')).toBe('fritter');
			expect(() => prog('--pork')).toThrow();
		});

		/**
		 * In this test, we create a program with a subcommand.
		 */
		test("a program with a subcommand", () => {
			let prog = program(
				subcommand('a',program()),
				() => {}
			);
			expect(typeof prog).toBe('function');
		});

		/**
		 * Test execution of a program.  In this case, our program is extremely
		 * simple:  the executable just returns "pork" (there's no definition of
		 * options or anything).
		 */
		test("test program execution", () => {
			let prog = program(() => 'pork');
			expect(prog()).toBe('pork');
		});

		/**
		 * The goal here is to test our option grabber in various ways (inside
		 * the program's runtime).
		 */
		test("getting an option during program execution", () => {
			let prog = program(
				option('-p --pig {String}'),
				($prog) => {
					expect($prog.getOption('pig')).toBe('dog');
					expect($prog.getOption('p')).toBe('dog');
					expect($prog.getOption('gibs')).toBe(undefined);
				}
			);
			prog('--pig', 'dog');

			// What to do if we don't get a required argument for an option.
			expect(() => prog('--pig')).toThrow();
		});

		/**
		 * Checking arguments.
		 */
		test("getting an argument during program execution", () => {
			let argval;
			let prog = program(
				option('--opt1 {String}'),
				argument('name', 'the name to use', 'enter any name'),
				$prog => {
					argval = $prog.getArgument('name');	
				}
			);

			prog('dog');
			expect(argval).toBe('dog');
		});

		/**
		 * Checking subcommands.
		 */
		test("getting an option during program execution", () => {
			let prog = program(
				option('--outer {String}'),
				subcommand('find', program(
					option('--inner {String}'),
					($prog) => {
						if ($prog.getOption('inner')) {
							return `inner is ${$prog.getOption('inner')}`;
						} else {
							return 'found something';
						}
					}
				)),
				option('--outer-after {String}'),
				$prog => {
					if ($prog.subcommand) {
						// For the time being, this is how you have to run a subcommand
						// if you want to pass in its args exactly as received.  The
						// caller can also mangle anything it wants.
						return $prog.subcommand.run(...$prog.subcommand.args);
					} else {
						return 'not looking';
					}
				}
			);

			expect(prog()).toBe('not looking');
			expect(prog('find')).toBe('found something');
			expect(prog('find', '--inner', 'funk')).toBe('inner is funk');
			expect(prog('dog')).toBe('not looking');
		});

		/**
		 * Implicit subcommand args
		 */
		test("getting an option during program execution", () => {
			let prog = program(
				option('--outer {String}'),
				subcommand('find', program(
					option('--inner {String}'),
					($prog) => {
						if ($prog.getOption('inner')) {
							return `inner is ${$prog.getOption('inner')}`;
						} else {
							return 'found something';
						}
					}
				)),
				option('--outer-after {String}'),
				$prog => {
					if ($prog.subcommand) {
						// For the time being, this is how you have to run a subcommand
						// if you want to pass in its args exactly as received.  The
						// caller can also mangle anything it wants.
						return $prog.subcommand.run();
					} else {
						return 'not looking';
					}
				}
			);

			expect(prog()).toBe('not looking');
			expect(prog('find')).toBe('found something');
			expect(prog('find', '--inner', 'funk')).toBe('inner is funk');
			expect(prog('dog')).toBe('not looking');
		});

	});

	/**
	 * Sad path tests describe cases in which the program gets input it
	 * doesn't like, but handles them gracefully.
	 *
	 * In HTTP terms, these are like 400-series statuses.
	 */
	describe("sad-path", () => {
		test("multiple entry points", () => {
			expect(() => program(() => {}, () => {})).toThrow(
				"Programs cannot have multiple handler functions."
			);
		});
	});
});

