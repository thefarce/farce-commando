import suite      from './suite-generator.js'
import subcommand from '../src/subcommand.js'
import option     from '../src/option.js'
import program    from '../src/program.js'
import info       from '../package.json'

suite([info.version, 'Subcommands'], () => {
  /**
   * Happy path describes tests that describe the program's execution when
   * everything is used as expected and things work well.
   *
   * In HTTP terms, these are like 200-series statuses.
   */
  describe("happy-path", () => {
    /*

    test("subcommand creation", () => {
      let sub = subcommand("pickle", program());
      expect(sub.$).toBe("subcommand");
    });
    */

    test("subcommand with options", () => {
      let sub  = subcommand(
        "sub",
        program(
          option('--test {String}'),
          $prog => {
            return $prog.getOption('test') || 'subby';
          }
        )
      );

      let prog = program(
        sub,
        ($prog) => {
          if ($prog.subcommand) {
            return $prog.subcommand.run();
          }
          return "proggy";
        }
      );

//      expect(sub()).toBe("subby");
//      expect(prog()).toBe("proggy");
//      expect(prog("sub")).toBe("subby");
      // This test currently fails.
      expect(prog("sub", "--test", "pork")).toBe("pork");
//      expect(prog("--test", "pork", "sub")).toThrow();
    });

  });

  /**
   * Sad path tests describe cases in which the program gets input it
   * doesn't like, but handles them gracefully.
   *
   * In HTTP terms, these are like 400-series statuses.
   */
  describe("sad-path", () => {
    test("no subcommand name", () => {
      expect(() => subcommand(program())).toThrow();
    });

    test("no subcommand args", () => {
      expect(() => subcommand()).toThrow();
    });

    test("null subcommand name", () => {
      expect(() => subcommand(null, program())).toThrow("Each");
    });

    test("no subcommand program", () => {
      expect(() => subcommand("subcommand")).toThrow();
    });

    test("subcommand is not a program", () => {
      expect(() => subcommand("subcommand", {})).toThrow();
    });

  });
});
