import info       from '../package.json'
import option     from '../src/option.js'
import program    from '../src/program.js'
import subcommand from '../src/subcommand.js'
import suite      from './suite-generator.js'

suite([info.version, 'help text'], () => {
	describe("rendering help", () => {
		let stderrContent;
		let stdErrWrite = process.stderr.write;

		beforeEach(() => {
			process.stderr.write = (...args) => (stderrContent = args.join(''));
		});

		afterEach(() => {
			process.stderr.write = stdErrWrite;
		});

		test("multiple entry points", () => {
			const prog = program(
				option('-a --apple'),
				() => {}
			);

			prog('--help');
			expect(stderrContent).toBe(
				'MANUAL: brief (--help brief|manual|technical|full)\n' +
				'\n' +
				'OPTIONS\n' +
				'    --help -h -? Displays help content\n' +
				'    --apple -a\n' +
				'\n'
			);
		});

		
		test("Technical help with no flags.", () => {
			const prog = program(
				option('-a --apple (a|b)'),
				option('-o --orange {String|Number}'),
				() => {}
			);

			prog('--help', 'technical');
			expect(stderrContent).toBe(
				'MANUAL: technical (--help brief|manual|technical|full)\n' +
          '\n' +
          'OPTIONS\n' +
          '    --help -h -? Displays help content\n' +
          '        This option is not required, may not be repeated, takes an argument (string) that must be one of [brief, manual, technical, full], and has a default value of "brief".\n' +
          '\n' +
          '    --apple -a \n' +
          '        This option is not required, may not be repeated, and does not take an argument.\n' +
          '\n' +
          '    --orange -o \n' +
          '        This option is not required, may not be repeated, and takes an argument (string | number).\n' +
          '\n'
			);
		});

		test("Option flags", () => {
			const prog = program(
				option('!+ -a --apple'),
				() => {}
			);

			prog('--help', 'technical');
			expect(stderrContent).toBe(
				'MANUAL: technical (--help brief|manual|technical|full)\n' +
          '\n' +
          'OPTIONS\n' +
          '    --help -h -? Displays help content\n' +
          '        This option is not required, may not be repeated, takes an argument (string) that must be one of [brief, manual, technical, full], and has a default value of "brief".\n' +
          '\n' +
          '    --apple -a \n' +
          '        This option is required, may not be repeated, and does not take an argument.\n' +
          '\n'
			);
		});

		test("Option flags", () => {
			const prog = program(
				option('!+ -a --apple'),
				() => {}
			);

			prog('--help', 'manual');
			expect(stderrContent).toBe(
				'MANUAL: manual (--help brief|manual|technical|full)\n' +
          '\n' +
          'OPTIONS\n' +
          '    --help -h -? Displays help content\n' +
          '        This option creates programmatically-formatted help for the program.\n' +
          '\n' +
          '    --apple -a\n' +
          '\n'
			);
		});

		test("Option flags", () => {
			const prog = program(
				option('!+ -a --apple'),
				() => {}
			);

			prog('--help', 'full');
			expect(stderrContent).toBe(
				'MANUAL: full (--help brief|manual|technical|full)\n' +
          '\n' +
          'OPTIONS\n' +
          '    --help -h -? Displays help content\n' +
          '        This option is not required, may not be repeated, takes an argument (string) that must be one of [brief, manual, technical, full], and has a default value of "brief".\n' +
          '\n' +
          '        This option creates programmatically-formatted help for the program.\n' +
          '\n' +
          '    --apple -a \n' +
          '        This option is required, may not be repeated, and does not take an argument.\n' +
          '\n'
			);
		});

		test("Option flags", () => {
			const prog = program(
				option('~ -a --apple'),
				() => {}
			);

			prog('--help');
			expect(stderrContent).toBe(
				'MANUAL: brief (--help brief|manual|technical|full)\n' +
				'\n' +
				'OPTIONS\n' +
				'    --help -h -? Displays help content\n' +
				'    --apple -a (DEPRECATED)\n' +
				'\n'
			);
		});

		test("the OVERVIEW element", () => {
			const prog = program(
				"Given an apple, do something.",

				option('-a --apple'),
				() => {}
			);

			prog('--help');
			expect(stderrContent).toBe(
				'MANUAL: brief (--help brief|manual|technical|full)\n' +
				'\n' +
				'OVERVIEW\n' +
				'Given an apple, do something.\n' +
				'\n' +
				'OPTIONS\n' +
				'    --help -h -? Displays help content\n' +
				'    --apple -a\n' +
				'\n'
			);
		});

	});
});

