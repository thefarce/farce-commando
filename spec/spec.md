
# Commando Specification

## Program Definition

Commando provides two primary interfaces for creating command line programs.
They are completely equivalent.

  1. Functional
  2. Object-Oriented

### Functional

In the functional approach, a program is generated using only function
calls.  All of the following snippets produce exactly the same program and
can be used with the following example execution.

```bash
# Assume the program created is called "prog".
$ prog
$ prog --count 4
$ prog -s -c 12
$ prog --silent -c 9
$ prog event --date 2020-12-14
# All Commando programs provide free help functionality.
$ prog --help
$ prog -h
$ prog -?
$ prog event --help
$ prog event -h
$ prog event -?
```

The following implementations are all exactly equivalent.

```javascript
import {program,option,subcommand} from '@thefarce/commando'

// Order of args doesn't matter
const main = program(
  option("-c --count {Integer} [0] The total number."),
  option("-s --silent {Boolean} Turns on silent mode."),
  subcommand("event", program(
    option("! -d --date {String} The date of the event."),
    (context) => console.log("event date"), 
  ),
  (context) => console.log("main") 
);
```

```javascript
import {attach,program,option,subcommand} from '@thefarce/commando'

const main = program();
attach(program, option("-c --count {Integer} [0] The total number."));
attach(program, option("-s --silent {Boolean} Turns on silent mode."));
attach(
  program,
  subcommand(
    "event",
    program(
      option("! -d --date {String} The date of the event."),
      (context) => console.log("event date"), 
    )
  )
);
attach(program, (context) => console.log("main"));
```

```javascript
import {program,option,attach,subcommand} from '@thefarce/commando'

const main = attach(
  program(),
  option("-c --count {Integer} [0] The total number."),
  option("-s --silent {Boolean} Turns on silent mode."),
  subcommand("event", program(
    option("! -d --date {String} The date of the event."),
    (context) => console.log("event date"), 
  ),
  (context) => console.log("main") 
);
```

## Program Invocation

Each program can be invoked as a function.  Using the definitions above, for
example, you can use the following invocations.

```javascript
import {program,option,subcommand} from '@thefarce/commando'

// Order of args doesn't matter
const main = program(...);

// Reads process.argv to determine invocation.
program();

// Ignores process.argv and uses the provided arg as though they were
entered through the command line.
program(["-c", 4, "--silent"]);
```

## Arguments

"Arguments" refers to the set of invocation inputs that are to be
interpreted by the program, _excluding_ subcommands and option flags.  This
does include option arguments.

Arguments should be processed in order.  Their interpretation should be
completely determined by:

  * options
  * subcommands
  * program entrypoint functions

## Options

A program's options are specified either as a string or a configuration
object.

### Object Specification

#### Flag Token

A _Flag Token_ is any string that can be used as an option flag.  Flag
tokens must match the following rules.

  * They MUST have 1-128 character.
  * They MUST NOT begin with a hyphen.
  * They MUST contain only these characters:  `[a-zA-Z0-9-_?:]`
  * Single-character flag tokens may not be a single hyphen, underscore, or
    colon.
  * Multi-character flag tokens may not include a question mark.

#### Type Specifier

A _Type Specifier_ is used to validate and restrict input.  These include:
  
  * String  | Str
  * Number  | Num
  * Integer | Int
  * Boolean | Bool

Option configuration objects are specified according to the following
schema.

```javascript
const option = {
  allowMultiple : <boolean>,
  default       : *,
  description   : <markdown>,
  rendered      : <string>,
  required      : <boolean>,
  tokens        : Array<Flag-Token>,
  types         : Array<Type-Specifier>,
  values        : Array<*>,
};
```

### String Specification

For convenience, developers may also specify the option in string form.
This is directly transformed into an Object Specification for internal use.

For example, the following are equivalent:

```javascript
// These two option declarations are equivalent.
.addOption("! -c --count --number :totalCount {Number|Int}+ (23,41,1.31) [41] The total number.")
.addOption({
  allowMultiple : true,
  default       : 41,
  description   : 'The total number',
  rendered      : 'totalCount',
  required      : true,
  tokens        : ['c','count','number'],
  types         : ['Number','Int'],
  values        : [23,41,1.31],
});
```

## Subcommands

Multiple programs can be combined using subcommand function call.
Subcommands are simply Commando programs mounted at string.

```javascript
import {program,option,subcommand} from '@thefarce/commando'

// Order of args doesn't matter
const main = program(
  (context) => console.log("main") 
  subcommand("a", program((context) => console.log("subcommand a")),
  subcommand("b", program((context) => console.log("subcommand b")),
);
```

## Interactive Shell

## ncurses





