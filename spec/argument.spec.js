import argument from '../src/argument.js';

describe('argument', () => {
  test('should create an argument object with the provided name, description, and help text', () => {
    const name = 'input';
    const desc = 'The input file';
    const help = 'Specify the input file path';

    const result = argument(name, desc, help);

    expect(result).toEqual({
      $: 'argument',
      spec: {
        name: 'input',
        desc: 'The input file',
        help: 'Specify the input file path'
      }
    });
  });

  test('should throw an error if the name is not provided', () => {
    expect(() => {
      argument();
    }).toThrow('Each argument must have a name.');
  });

  test('should throw an error if the name is a program object', () => {
    const program = {
      $: 'program',
      spec: {
        name: 'my-program',
        desc: 'My program',
        help: 'This is my program'
      }
    };

    expect(() => {
      argument(program);
    }).toThrow('Each argument must have a name.');
  });
});
