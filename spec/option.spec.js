import suite  from './suite-generator.js'
import option from '../src/option.js'
import info   from '../package.json'

/**
 * Options.
 *
 * There are lots of ways to build options.  Because there are so many,
 * there are even more total possible combinations.  This attempts to be a
 * pretty robust catalog of possible options combinations to test them
 * thoroughly.
 *
 * The big deal here is to define each kind of parameter, then create our
 * many combinations.
 */

/**
 * isRequired
 * Boolean.
 * Can occur zero or one time.
 * Whether or not the option is required.
 * Represented by an exclaimation point in the "flags" section of the option
 * string.
 * Example:  ! ...
 */

/**
 * allowMultiple
 * Boolean.
 * Can occur zero or one time.
 * Can this option occur multiple times.
 * Represented by a plus in the "flags" section of the option string.
 * Example:  + ...
 */

/**
 * short-token
 * String (starts with a single hyphen).
 * Can appear any number of times.
 * A one-character token for this option.
 * Represented as a single character prefixed by a single hyphen.
 * Example:  -c
 */

/**
 * long-token
 * String (starts with a double hyphen).
 * Can appear multiple times.
 * A multi-character token for this option.
 * Represented as multiple characters prefixed by a double hyphen.
 * Example:  --color
 */

/**
 * type
 * (String|Int|Number|Boolean|...)
 * Can have multiple values (pipe-delimited).
 * A string indicating the type the option's value should take.
 * Represented by a valid type enclosed in set braces.
 * Example:  {String|Int}
 */

/**
 * options
 * List.
 * Can have zero or one occurrence.
 * A string of pipe-delimited values enclosed in parentheses.
 * Example:  (red|blue|green)
 */

/**
 * default
 * Any
 * Can have zero or one occurrence.
 * A default value if the option is omitted.
 * Represented by pipe-delimited strings enclosed in square brackets.
 */

/**
 * description
 * Text
 * Can have one occurrence.
 * A description of the option.
 * Arbitrary Markdown content describing the option's use and
 * interpretation.
 */

suite([info.version, 'Options'], () => {
  describe("parametric option building", () => {


    test('--color [red]', () => {
      expect(option("--color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "default": "red"
        });
    });


    test('--color (red|blue)', () => {
      expect(option("--color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color (red|blue) [red]', () => {
      expect(option("--color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color {String}', () => {
      expect(option("--color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });


    test('--color {String} [red]', () => {
      expect(option("--color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });


    test('--color {String} (red|blue)', () => {
      expect(option("--color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color {String} (red|blue) [red]', () => {
      expect(option("--color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color {Integer}', () => {
      expect(option("--color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });

    test('--color {Integer} [red]', () => {
      expect(option("--color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });


    test('--color {Integer} (red|blue)', () => {
      expect(option("--color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color {Integer} (red|blue) [red]', () => {
      expect(option("--color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color {Number}', () => {
      expect(option("--color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });


    test('--color {Number} [red]', () => {
      expect(option("--color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });


    test('--color {Number} (red|blue)', () => {
      expect(option("--color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color {Number} (red|blue) [red]', () => {
      expect(option("--color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color {Boolean}', () => {
      expect(option("--color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });


    test('--color {Boolean} [red]', () => {
      expect(option("--color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });


    test('--color {Boolean} (red|blue)', () => {
      expect(option("--color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color {Boolean} (red|blue) [red]', () => {
      expect(option("--color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color --set-color', () => {
      expect(option("--color --set-color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ]
        });
    });


    test('--color --set-color [red]', () => {
      expect(option("--color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });


    test('--color --set-color (red|blue)', () => {
      expect(option("--color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color --set-color (red|blue) [red]', () => {
      expect(option("--color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {String}', () => {
      expect(option("--color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });


    test('--color --set-color {String} [red]', () => {
      expect(option("--color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {String} (red|blue)', () => {
      expect(option("--color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color --set-color {String} (red|blue) [red]', () => {
      expect(option("--color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {Integer}', () => {
      expect(option("--color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });


    test('--color --set-color {Integer} [red]', () => {
      expect(option("--color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {Integer} (red|blue)', () => {
      expect(option("--color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("--color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {Number}', () => {
      expect(option("--color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });


    test('--color --set-color {Number} [red]', () => {
      expect(option("--color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {Number} (red|blue)', () => {
      expect(option("--color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color --set-color {Number} (red|blue) [red]', () => {
      expect(option("--color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {Boolean}', () => {
      expect(option("--color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });


    test('--color --set-color {Boolean} [red]', () => {
      expect(option("--color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });


    test('--color --set-color {Boolean} (red|blue)', () => {
      expect(option("--color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });


    test('--color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("--color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c', () => {
      expect(option("-c").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ]
        });
    });
  

    test('-c [red]', () => {
      expect(option("-c [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "default": "red"
        });
    });
  

    test('-c (red|blue)', () => {
      expect(option("-c (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c (red|blue) [red]', () => {
      expect(option("-c (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c {String}', () => {
      expect(option("-c {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('-c {String} [red]', () => {
      expect(option("-c {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('-c {String} (red|blue)', () => {
      expect(option("-c {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c {String} (red|blue) [red]', () => {
      expect(option("-c {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c {Integer}', () => {
      expect(option("-c {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('-c {Integer} [red]', () => {
      expect(option("-c {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('-c {Integer} (red|blue)', () => {
      expect(option("-c {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c {Integer} (red|blue) [red]', () => {
      expect(option("-c {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c {Number}', () => {
      expect(option("-c {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('-c {Number} [red]', () => {
      expect(option("-c {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('-c {Number} (red|blue)', () => {
      expect(option("-c {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c {Number} (red|blue) [red]', () => {
      expect(option("-c {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c {Boolean}', () => {
      expect(option("-c {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('-c {Boolean} [red]', () => {
      expect(option("-c {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('-c {Boolean} (red|blue)', () => {
      expect(option("-c {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c {Boolean} (red|blue) [red]', () => {
      expect(option("-c {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color', () => {
      expect(option("-c --color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ]
        });
    });
  

    test('-c --color [red]', () => {
      expect(option("-c --color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('-c --color (red|blue)', () => {
      expect(option("-c --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color (red|blue) [red]', () => {
      expect(option("-c --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {String}', () => {
      expect(option("-c --color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('-c --color {String} [red]', () => {
      expect(option("-c --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {String} (red|blue)', () => {
      expect(option("-c --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color {String} (red|blue) [red]', () => {
      expect(option("-c --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {Integer}', () => {
      expect(option("-c --color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('-c --color {Integer} [red]', () => {
      expect(option("-c --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {Integer} (red|blue)', () => {
      expect(option("-c --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color {Integer} (red|blue) [red]', () => {
      expect(option("-c --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {Number}', () => {
      expect(option("-c --color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('-c --color {Number} [red]', () => {
      expect(option("-c --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {Number} (red|blue)', () => {
      expect(option("-c --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color {Number} (red|blue) [red]', () => {
      expect(option("-c --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {Boolean}', () => {
      expect(option("-c --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('-c --color {Boolean} [red]', () => {
      expect(option("-c --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('-c --color {Boolean} (red|blue)', () => {
      expect(option("-c --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color {Boolean} (red|blue) [red]', () => {
      expect(option("-c --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color', () => {
      expect(option("-c --color --set-color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ]
        });
    });
  

    test('-c --color --set-color [red]', () => {
      expect(option("-c --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color (red|blue)', () => {
      expect(option("-c --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color --set-color (red|blue) [red]', () => {
      expect(option("-c --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {String}', () => {
      expect(option("-c --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('-c --color --set-color {String} [red]', () => {
      expect(option("-c --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {String} (red|blue)', () => {
      expect(option("-c --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color --set-color {String} (red|blue) [red]', () => {
      expect(option("-c --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {Integer}', () => {
      expect(option("-c --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('-c --color --set-color {Integer} [red]', () => {
      expect(option("-c --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {Integer} (red|blue)', () => {
      expect(option("-c --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("-c --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {Number}', () => {
      expect(option("-c --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('-c --color --set-color {Number} [red]', () => {
      expect(option("-c --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {Number} (red|blue)', () => {
      expect(option("-c --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("-c --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {Boolean}', () => {
      expect(option("-c --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('-c --color --set-color {Boolean} [red]', () => {
      expect(option("-c --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('-c --color --set-color {Boolean} (red|blue)', () => {
      expect(option("-c --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("-c --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r', () => {
      expect(option("-c -r").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ]
        });
    });
  

    test('-c -r [red]', () => {
      expect(option("-c -r [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "default": "red"
        });
    });
  

    test('-c -r (red|blue)', () => {
      expect(option("-c -r (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r (red|blue) [red]', () => {
      expect(option("-c -r (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {String}', () => {
      expect(option("-c -r {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('-c -r {String} [red]', () => {
      expect(option("-c -r {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {String} (red|blue)', () => {
      expect(option("-c -r {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r {String} (red|blue) [red]', () => {
      expect(option("-c -r {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {Integer}', () => {
      expect(option("-c -r {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('-c -r {Integer} [red]', () => {
      expect(option("-c -r {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {Integer} (red|blue)', () => {
      expect(option("-c -r {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r {Integer} (red|blue) [red]', () => {
      expect(option("-c -r {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {Number}', () => {
      expect(option("-c -r {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('-c -r {Number} [red]', () => {
      expect(option("-c -r {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {Number} (red|blue)', () => {
      expect(option("-c -r {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r {Number} (red|blue) [red]', () => {
      expect(option("-c -r {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {Boolean}', () => {
      expect(option("-c -r {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('-c -r {Boolean} [red]', () => {
      expect(option("-c -r {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('-c -r {Boolean} (red|blue)', () => {
      expect(option("-c -r {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r {Boolean} (red|blue) [red]', () => {
      expect(option("-c -r {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color', () => {
      expect(option("-c -r --color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ]
        });
    });
  

    test('-c -r --color [red]', () => {
      expect(option("-c -r --color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color (red|blue)', () => {
      expect(option("-c -r --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color (red|blue) [red]', () => {
      expect(option("-c -r --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {String}', () => {
      expect(option("-c -r --color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('-c -r --color {String} [red]', () => {
      expect(option("-c -r --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {String} (red|blue)', () => {
      expect(option("-c -r --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color {String} (red|blue) [red]', () => {
      expect(option("-c -r --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {Integer}', () => {
      expect(option("-c -r --color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('-c -r --color {Integer} [red]', () => {
      expect(option("-c -r --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {Integer} (red|blue)', () => {
      expect(option("-c -r --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color {Integer} (red|blue) [red]', () => {
      expect(option("-c -r --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {Number}', () => {
      expect(option("-c -r --color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('-c -r --color {Number} [red]', () => {
      expect(option("-c -r --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {Number} (red|blue)', () => {
      expect(option("-c -r --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color {Number} (red|blue) [red]', () => {
      expect(option("-c -r --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {Boolean}', () => {
      expect(option("-c -r --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('-c -r --color {Boolean} [red]', () => {
      expect(option("-c -r --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color {Boolean} (red|blue)', () => {
      expect(option("-c -r --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color {Boolean} (red|blue) [red]', () => {
      expect(option("-c -r --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color', () => {
      expect(option("-c -r --color --set-color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ]
        });
    });
  

    test('-c -r --color --set-color [red]', () => {
      expect(option("-c -r --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color (red|blue)', () => {
      expect(option("-c -r --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color --set-color (red|blue) [red]', () => {
      expect(option("-c -r --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {String}', () => {
      expect(option("-c -r --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('-c -r --color --set-color {String} [red]', () => {
      expect(option("-c -r --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {String} (red|blue)', () => {
      expect(option("-c -r --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color --set-color {String} (red|blue) [red]', () => {
      expect(option("-c -r --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {Integer}', () => {
      expect(option("-c -r --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('-c -r --color --set-color {Integer} [red]', () => {
      expect(option("-c -r --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {Integer} (red|blue)', () => {
      expect(option("-c -r --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("-c -r --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {Number}', () => {
      expect(option("-c -r --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('-c -r --color --set-color {Number} [red]', () => {
      expect(option("-c -r --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {Number} (red|blue)', () => {
      expect(option("-c -r --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("-c -r --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {Boolean}', () => {
      expect(option("-c -r --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('-c -r --color --set-color {Boolean} [red]', () => {
      expect(option("-c -r --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('-c -r --color --set-color {Boolean} (red|blue)', () => {
      expect(option("-c -r --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('-c -r --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("-c -r --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    // [object Object]
    test('!', () => {
      expect(() => option("!"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! [red]', () => {
      expect(() => option("! [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! (red|blue)', () => {
      expect(() => option("! (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! (red|blue) [red]', () => {
      expect(() => option("! (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {String}', () => {
      expect(() => option("! {String}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {String} [red]', () => {
      expect(() => option("! {String} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {String} (red|blue)', () => {
      expect(() => option("! {String} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {String} (red|blue) [red]', () => {
      expect(() => option("! {String} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Integer}', () => {
      expect(() => option("! {Integer}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Integer} [red]', () => {
      expect(() => option("! {Integer} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Integer} (red|blue)', () => {
      expect(() => option("! {Integer} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Integer} (red|blue) [red]', () => {
      expect(() => option("! {Integer} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Number}', () => {
      expect(() => option("! {Number}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Number} [red]', () => {
      expect(() => option("! {Number} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Number} (red|blue)', () => {
      expect(() => option("! {Number} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Number} (red|blue) [red]', () => {
      expect(() => option("! {Number} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Boolean}', () => {
      expect(() => option("! {Boolean}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Boolean} [red]', () => {
      expect(() => option("! {Boolean} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Boolean} (red|blue)', () => {
      expect(() => option("! {Boolean} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('! {Boolean} (red|blue) [red]', () => {
      expect(() => option("! {Boolean} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    test('! --color', () => {
      expect(option("! --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ]
        });
    });
  

    test('! --color [red]', () => {
      expect(option("! --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "default": "red"
        });
    });
  

    test('! --color (red|blue)', () => {
      expect(option("! --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color (red|blue) [red]', () => {
      expect(option("! --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color {String}', () => {
      expect(option("! --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! --color {String} [red]', () => {
      expect(option("! --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! --color {String} (red|blue)', () => {
      expect(option("! --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color {String} (red|blue) [red]', () => {
      expect(option("! --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color {Integer}', () => {
      expect(option("! --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! --color {Integer} [red]', () => {
      expect(option("! --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! --color {Integer} (red|blue)', () => {
      expect(option("! --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color {Integer} (red|blue) [red]', () => {
      expect(option("! --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color {Number}', () => {
      expect(option("! --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! --color {Number} [red]', () => {
      expect(option("! --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! --color {Number} (red|blue)', () => {
      expect(option("! --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color {Number} (red|blue) [red]', () => {
      expect(option("! --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color {Boolean}', () => {
      expect(option("! --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! --color {Boolean} [red]', () => {
      expect(option("! --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! --color {Boolean} (red|blue)', () => {
      expect(option("! --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color {Boolean} (red|blue) [red]', () => {
      expect(option("! --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color', () => {
      expect(option("! --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ]
        });
    });
  

    test('! --color --set-color [red]', () => {
      expect(option("! --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color (red|blue)', () => {
      expect(option("! --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color --set-color (red|blue) [red]', () => {
      expect(option("! --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {String}', () => {
      expect(option("! --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! --color --set-color {String} [red]', () => {
      expect(option("! --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {String} (red|blue)', () => {
      expect(option("! --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color --set-color {String} (red|blue) [red]', () => {
      expect(option("! --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {Integer}', () => {
      expect(option("! --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! --color --set-color {Integer} [red]', () => {
      expect(option("! --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {Integer} (red|blue)', () => {
      expect(option("! --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("! --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {Number}', () => {
      expect(option("! --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! --color --set-color {Number} [red]', () => {
      expect(option("! --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {Number} (red|blue)', () => {
      expect(option("! --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("! --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {Boolean}', () => {
      expect(option("! --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! --color --set-color {Boolean} [red]', () => {
      expect(option("! --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! --color --set-color {Boolean} (red|blue)', () => {
      expect(option("! --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("! --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c', () => {
      expect(option("! -c").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ]
        });
    });
  

    test('! -c [red]', () => {
      expect(option("! -c [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "default": "red"
        });
    });
  

    test('! -c (red|blue)', () => {
      expect(option("! -c (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c (red|blue) [red]', () => {
      expect(option("! -c (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c {String}', () => {
      expect(option("! -c {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! -c {String} [red]', () => {
      expect(option("! -c {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! -c {String} (red|blue)', () => {
      expect(option("! -c {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c {String} (red|blue) [red]', () => {
      expect(option("! -c {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c {Integer}', () => {
      expect(option("! -c {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! -c {Integer} [red]', () => {
      expect(option("! -c {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! -c {Integer} (red|blue)', () => {
      expect(option("! -c {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c {Integer} (red|blue) [red]', () => {
      expect(option("! -c {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c {Number}', () => {
      expect(option("! -c {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! -c {Number} [red]', () => {
      expect(option("! -c {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! -c {Number} (red|blue)', () => {
      expect(option("! -c {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c {Number} (red|blue) [red]', () => {
      expect(option("! -c {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c {Boolean}', () => {
      expect(option("! -c {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! -c {Boolean} [red]', () => {
      expect(option("! -c {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! -c {Boolean} (red|blue)', () => {
      expect(option("! -c {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c {Boolean} (red|blue) [red]', () => {
      expect(option("! -c {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color', () => {
      expect(option("! -c --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ]
        });
    });
  

    test('! -c --color [red]', () => {
      expect(option("! -c --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color (red|blue)', () => {
      expect(option("! -c --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color (red|blue) [red]', () => {
      expect(option("! -c --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {String}', () => {
      expect(option("! -c --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! -c --color {String} [red]', () => {
      expect(option("! -c --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {String} (red|blue)', () => {
      expect(option("! -c --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color {String} (red|blue) [red]', () => {
      expect(option("! -c --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {Integer}', () => {
      expect(option("! -c --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! -c --color {Integer} [red]', () => {
      expect(option("! -c --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {Integer} (red|blue)', () => {
      expect(option("! -c --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color {Integer} (red|blue) [red]', () => {
      expect(option("! -c --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {Number}', () => {
      expect(option("! -c --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! -c --color {Number} [red]', () => {
      expect(option("! -c --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {Number} (red|blue)', () => {
      expect(option("! -c --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color {Number} (red|blue) [red]', () => {
      expect(option("! -c --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {Boolean}', () => {
      expect(option("! -c --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! -c --color {Boolean} [red]', () => {
      expect(option("! -c --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color {Boolean} (red|blue)', () => {
      expect(option("! -c --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color {Boolean} (red|blue) [red]', () => {
      expect(option("! -c --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color', () => {
      expect(option("! -c --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ]
        });
    });
  

    test('! -c --color --set-color [red]', () => {
      expect(option("! -c --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color (red|blue)', () => {
      expect(option("! -c --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color --set-color (red|blue) [red]', () => {
      expect(option("! -c --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {String}', () => {
      expect(option("! -c --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! -c --color --set-color {String} [red]', () => {
      expect(option("! -c --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {String} (red|blue)', () => {
      expect(option("! -c --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color --set-color {String} (red|blue) [red]', () => {
      expect(option("! -c --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {Integer}', () => {
      expect(option("! -c --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! -c --color --set-color {Integer} [red]', () => {
      expect(option("! -c --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {Integer} (red|blue)', () => {
      expect(option("! -c --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("! -c --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {Number}', () => {
      expect(option("! -c --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! -c --color --set-color {Number} [red]', () => {
      expect(option("! -c --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {Number} (red|blue)', () => {
      expect(option("! -c --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("! -c --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {Boolean}', () => {
      expect(option("! -c --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! -c --color --set-color {Boolean} [red]', () => {
      expect(option("! -c --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! -c --color --set-color {Boolean} (red|blue)', () => {
      expect(option("! -c --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("! -c --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r', () => {
      expect(option("! -c -r").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ]
        });
    });
  

    test('! -c -r [red]', () => {
      expect(option("! -c -r [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r (red|blue)', () => {
      expect(option("! -c -r (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r (red|blue) [red]', () => {
      expect(option("! -c -r (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {String}', () => {
      expect(option("! -c -r {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! -c -r {String} [red]', () => {
      expect(option("! -c -r {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {String} (red|blue)', () => {
      expect(option("! -c -r {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r {String} (red|blue) [red]', () => {
      expect(option("! -c -r {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {Integer}', () => {
      expect(option("! -c -r {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! -c -r {Integer} [red]', () => {
      expect(option("! -c -r {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {Integer} (red|blue)', () => {
      expect(option("! -c -r {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r {Integer} (red|blue) [red]', () => {
      expect(option("! -c -r {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {Number}', () => {
      expect(option("! -c -r {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! -c -r {Number} [red]', () => {
      expect(option("! -c -r {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {Number} (red|blue)', () => {
      expect(option("! -c -r {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r {Number} (red|blue) [red]', () => {
      expect(option("! -c -r {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {Boolean}', () => {
      expect(option("! -c -r {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! -c -r {Boolean} [red]', () => {
      expect(option("! -c -r {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r {Boolean} (red|blue)', () => {
      expect(option("! -c -r {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r {Boolean} (red|blue) [red]', () => {
      expect(option("! -c -r {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color', () => {
      expect(option("! -c -r --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ]
        });
    });
  

    test('! -c -r --color [red]', () => {
      expect(option("! -c -r --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color (red|blue)', () => {
      expect(option("! -c -r --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color (red|blue) [red]', () => {
      expect(option("! -c -r --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {String}', () => {
      expect(option("! -c -r --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! -c -r --color {String} [red]', () => {
      expect(option("! -c -r --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {String} (red|blue)', () => {
      expect(option("! -c -r --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color {String} (red|blue) [red]', () => {
      expect(option("! -c -r --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {Integer}', () => {
      expect(option("! -c -r --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! -c -r --color {Integer} [red]', () => {
      expect(option("! -c -r --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {Integer} (red|blue)', () => {
      expect(option("! -c -r --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color {Integer} (red|blue) [red]', () => {
      expect(option("! -c -r --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {Number}', () => {
      expect(option("! -c -r --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! -c -r --color {Number} [red]', () => {
      expect(option("! -c -r --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {Number} (red|blue)', () => {
      expect(option("! -c -r --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color {Number} (red|blue) [red]', () => {
      expect(option("! -c -r --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {Boolean}', () => {
      expect(option("! -c -r --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! -c -r --color {Boolean} [red]', () => {
      expect(option("! -c -r --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color {Boolean} (red|blue)', () => {
      expect(option("! -c -r --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color {Boolean} (red|blue) [red]', () => {
      expect(option("! -c -r --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color', () => {
      expect(option("! -c -r --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ]
        });
    });
  

    test('! -c -r --color --set-color [red]', () => {
      expect(option("! -c -r --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color (red|blue)', () => {
      expect(option("! -c -r --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color --set-color (red|blue) [red]', () => {
      expect(option("! -c -r --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {String}', () => {
      expect(option("! -c -r --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('! -c -r --color --set-color {String} [red]', () => {
      expect(option("! -c -r --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {String} (red|blue)', () => {
      expect(option("! -c -r --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color --set-color {String} (red|blue) [red]', () => {
      expect(option("! -c -r --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {Integer}', () => {
      expect(option("! -c -r --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('! -c -r --color --set-color {Integer} [red]', () => {
      expect(option("! -c -r --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {Integer} (red|blue)', () => {
      expect(option("! -c -r --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("! -c -r --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {Number}', () => {
      expect(option("! -c -r --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('! -c -r --color --set-color {Number} [red]', () => {
      expect(option("! -c -r --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {Number} (red|blue)', () => {
      expect(option("! -c -r --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("! -c -r --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {Boolean}', () => {
      expect(option("! -c -r --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('! -c -r --color --set-color {Boolean} [red]', () => {
      expect(option("! -c -r --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('! -c -r --color --set-color {Boolean} (red|blue)', () => {
      expect(option("! -c -r --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('! -c -r --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("! -c -r --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    // [object Object]
    test('+', () => {
      expect(() => option("+"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ [red]', () => {
      expect(() => option("+ [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ (red|blue)', () => {
      expect(() => option("+ (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ (red|blue) [red]', () => {
      expect(() => option("+ (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {String}', () => {
      expect(() => option("+ {String}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {String} [red]', () => {
      expect(() => option("+ {String} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {String} (red|blue)', () => {
      expect(() => option("+ {String} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {String} (red|blue) [red]', () => {
      expect(() => option("+ {String} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Integer}', () => {
      expect(() => option("+ {Integer}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Integer} [red]', () => {
      expect(() => option("+ {Integer} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Integer} (red|blue)', () => {
      expect(() => option("+ {Integer} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Integer} (red|blue) [red]', () => {
      expect(() => option("+ {Integer} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Number}', () => {
      expect(() => option("+ {Number}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Number} [red]', () => {
      expect(() => option("+ {Number} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Number} (red|blue)', () => {
      expect(() => option("+ {Number} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Number} (red|blue) [red]', () => {
      expect(() => option("+ {Number} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Boolean}', () => {
      expect(() => option("+ {Boolean}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Boolean} [red]', () => {
      expect(() => option("+ {Boolean} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Boolean} (red|blue)', () => {
      expect(() => option("+ {Boolean} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('+ {Boolean} (red|blue) [red]', () => {
      expect(() => option("+ {Boolean} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    test('+ --color', () => {
      expect(option("+ --color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ]
        });
    });
  

    test('+ --color [red]', () => {
      expect(option("+ --color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "default": "red"
        });
    });
  

    test('+ --color (red|blue)', () => {
      expect(option("+ --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color (red|blue) [red]', () => {
      expect(option("+ --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {String}', () => {
      expect(option("+ --color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ --color {String} [red]', () => {
      expect(option("+ --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {String} (red|blue)', () => {
      expect(option("+ --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color {String} (red|blue) [red]', () => {
      expect(option("+ --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {Integer}', () => {
      expect(option("+ --color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ --color {Integer} [red]', () => {
      expect(option("+ --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {Integer} (red|blue)', () => {
      expect(option("+ --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color {Integer} (red|blue) [red]', () => {
      expect(option("+ --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {Number}', () => {
      expect(option("+ --color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ --color {Number} [red]', () => {
      expect(option("+ --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {Number} (red|blue)', () => {
      expect(option("+ --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color {Number} (red|blue) [red]', () => {
      expect(option("+ --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {Boolean}', () => {
      expect(option("+ --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ --color {Boolean} [red]', () => {
      expect(option("+ --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ --color {Boolean} (red|blue)', () => {
      expect(option("+ --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color {Boolean} (red|blue) [red]', () => {
      expect(option("+ --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color', () => {
      expect(option("+ --color --set-color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ]
        });
    });
  

    test('+ --color --set-color [red]', () => {
      expect(option("+ --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color (red|blue)', () => {
      expect(option("+ --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color --set-color (red|blue) [red]', () => {
      expect(option("+ --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {String}', () => {
      expect(option("+ --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ --color --set-color {String} [red]', () => {
      expect(option("+ --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {String} (red|blue)', () => {
      expect(option("+ --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color --set-color {String} (red|blue) [red]', () => {
      expect(option("+ --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {Integer}', () => {
      expect(option("+ --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ --color --set-color {Integer} [red]', () => {
      expect(option("+ --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {Integer} (red|blue)', () => {
      expect(option("+ --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("+ --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {Number}', () => {
      expect(option("+ --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ --color --set-color {Number} [red]', () => {
      expect(option("+ --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {Number} (red|blue)', () => {
      expect(option("+ --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("+ --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {Boolean}', () => {
      expect(option("+ --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ --color --set-color {Boolean} [red]', () => {
      expect(option("+ --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ --color --set-color {Boolean} (red|blue)', () => {
      expect(option("+ --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("+ --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c', () => {
      expect(option("+ -c").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ]
        });
    });
  

    test('+ -c [red]', () => {
      expect(option("+ -c [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "default": "red"
        });
    });
  

    test('+ -c (red|blue)', () => {
      expect(option("+ -c (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c (red|blue) [red]', () => {
      expect(option("+ -c (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {String}', () => {
      expect(option("+ -c {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ -c {String} [red]', () => {
      expect(option("+ -c {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {String} (red|blue)', () => {
      expect(option("+ -c {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c {String} (red|blue) [red]', () => {
      expect(option("+ -c {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {Integer}', () => {
      expect(option("+ -c {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ -c {Integer} [red]', () => {
      expect(option("+ -c {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {Integer} (red|blue)', () => {
      expect(option("+ -c {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c {Integer} (red|blue) [red]', () => {
      expect(option("+ -c {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {Number}', () => {
      expect(option("+ -c {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ -c {Number} [red]', () => {
      expect(option("+ -c {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {Number} (red|blue)', () => {
      expect(option("+ -c {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c {Number} (red|blue) [red]', () => {
      expect(option("+ -c {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {Boolean}', () => {
      expect(option("+ -c {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ -c {Boolean} [red]', () => {
      expect(option("+ -c {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ -c {Boolean} (red|blue)', () => {
      expect(option("+ -c {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c {Boolean} (red|blue) [red]', () => {
      expect(option("+ -c {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color', () => {
      expect(option("+ -c --color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ]
        });
    });
  

    test('+ -c --color [red]', () => {
      expect(option("+ -c --color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color (red|blue)', () => {
      expect(option("+ -c --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color (red|blue) [red]', () => {
      expect(option("+ -c --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {String}', () => {
      expect(option("+ -c --color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ -c --color {String} [red]', () => {
      expect(option("+ -c --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {String} (red|blue)', () => {
      expect(option("+ -c --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color {String} (red|blue) [red]', () => {
      expect(option("+ -c --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {Integer}', () => {
      expect(option("+ -c --color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ -c --color {Integer} [red]', () => {
      expect(option("+ -c --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {Integer} (red|blue)', () => {
      expect(option("+ -c --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color {Integer} (red|blue) [red]', () => {
      expect(option("+ -c --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {Number}', () => {
      expect(option("+ -c --color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ -c --color {Number} [red]', () => {
      expect(option("+ -c --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {Number} (red|blue)', () => {
      expect(option("+ -c --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color {Number} (red|blue) [red]', () => {
      expect(option("+ -c --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {Boolean}', () => {
      expect(option("+ -c --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ -c --color {Boolean} [red]', () => {
      expect(option("+ -c --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color {Boolean} (red|blue)', () => {
      expect(option("+ -c --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color {Boolean} (red|blue) [red]', () => {
      expect(option("+ -c --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color', () => {
      expect(option("+ -c --color --set-color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ]
        });
    });
  

    test('+ -c --color --set-color [red]', () => {
      expect(option("+ -c --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color (red|blue)', () => {
      expect(option("+ -c --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color --set-color (red|blue) [red]', () => {
      expect(option("+ -c --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {String}', () => {
      expect(option("+ -c --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ -c --color --set-color {String} [red]', () => {
      expect(option("+ -c --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {String} (red|blue)', () => {
      expect(option("+ -c --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color --set-color {String} (red|blue) [red]', () => {
      expect(option("+ -c --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {Integer}', () => {
      expect(option("+ -c --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ -c --color --set-color {Integer} [red]', () => {
      expect(option("+ -c --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {Integer} (red|blue)', () => {
      expect(option("+ -c --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("+ -c --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {Number}', () => {
      expect(option("+ -c --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ -c --color --set-color {Number} [red]', () => {
      expect(option("+ -c --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {Number} (red|blue)', () => {
      expect(option("+ -c --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("+ -c --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {Boolean}', () => {
      expect(option("+ -c --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ -c --color --set-color {Boolean} [red]', () => {
      expect(option("+ -c --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ -c --color --set-color {Boolean} (red|blue)', () => {
      expect(option("+ -c --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("+ -c --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r', () => {
      expect(option("+ -c -r").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ]
        });
    });
  

    test('+ -c -r [red]', () => {
      expect(option("+ -c -r [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r (red|blue)', () => {
      expect(option("+ -c -r (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r (red|blue) [red]', () => {
      expect(option("+ -c -r (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {String}', () => {
      expect(option("+ -c -r {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ -c -r {String} [red]', () => {
      expect(option("+ -c -r {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {String} (red|blue)', () => {
      expect(option("+ -c -r {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r {String} (red|blue) [red]', () => {
      expect(option("+ -c -r {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {Integer}', () => {
      expect(option("+ -c -r {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ -c -r {Integer} [red]', () => {
      expect(option("+ -c -r {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {Integer} (red|blue)', () => {
      expect(option("+ -c -r {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r {Integer} (red|blue) [red]', () => {
      expect(option("+ -c -r {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {Number}', () => {
      expect(option("+ -c -r {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ -c -r {Number} [red]', () => {
      expect(option("+ -c -r {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {Number} (red|blue)', () => {
      expect(option("+ -c -r {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r {Number} (red|blue) [red]', () => {
      expect(option("+ -c -r {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {Boolean}', () => {
      expect(option("+ -c -r {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ -c -r {Boolean} [red]', () => {
      expect(option("+ -c -r {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r {Boolean} (red|blue)', () => {
      expect(option("+ -c -r {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r {Boolean} (red|blue) [red]', () => {
      expect(option("+ -c -r {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color', () => {
      expect(option("+ -c -r --color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ]
        });
    });
  

    test('+ -c -r --color [red]', () => {
      expect(option("+ -c -r --color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color (red|blue)', () => {
      expect(option("+ -c -r --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color (red|blue) [red]', () => {
      expect(option("+ -c -r --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {String}', () => {
      expect(option("+ -c -r --color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ -c -r --color {String} [red]', () => {
      expect(option("+ -c -r --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {String} (red|blue)', () => {
      expect(option("+ -c -r --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color {String} (red|blue) [red]', () => {
      expect(option("+ -c -r --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {Integer}', () => {
      expect(option("+ -c -r --color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ -c -r --color {Integer} [red]', () => {
      expect(option("+ -c -r --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {Integer} (red|blue)', () => {
      expect(option("+ -c -r --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color {Integer} (red|blue) [red]', () => {
      expect(option("+ -c -r --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {Number}', () => {
      expect(option("+ -c -r --color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ -c -r --color {Number} [red]', () => {
      expect(option("+ -c -r --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {Number} (red|blue)', () => {
      expect(option("+ -c -r --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color {Number} (red|blue) [red]', () => {
      expect(option("+ -c -r --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {Boolean}', () => {
      expect(option("+ -c -r --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ -c -r --color {Boolean} [red]', () => {
      expect(option("+ -c -r --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color {Boolean} (red|blue)', () => {
      expect(option("+ -c -r --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color {Boolean} (red|blue) [red]', () => {
      expect(option("+ -c -r --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color', () => {
      expect(option("+ -c -r --color --set-color").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ]
        });
    });
  

    test('+ -c -r --color --set-color [red]', () => {
      expect(option("+ -c -r --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color (red|blue)', () => {
      expect(option("+ -c -r --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color --set-color (red|blue) [red]', () => {
      expect(option("+ -c -r --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {String}', () => {
      expect(option("+ -c -r --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {String} [red]', () => {
      expect(option("+ -c -r --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {String} (red|blue)', () => {
      expect(option("+ -c -r --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {String} (red|blue) [red]', () => {
      expect(option("+ -c -r --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {Integer}', () => {
      expect(option("+ -c -r --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {Integer} [red]', () => {
      expect(option("+ -c -r --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {Integer} (red|blue)', () => {
      expect(option("+ -c -r --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("+ -c -r --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {Number}', () => {
      expect(option("+ -c -r --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {Number} [red]', () => {
      expect(option("+ -c -r --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {Number} (red|blue)', () => {
      expect(option("+ -c -r --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("+ -c -r --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {Boolean}', () => {
      expect(option("+ -c -r --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {Boolean} [red]', () => {
      expect(option("+ -c -r --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('+ -c -r --color --set-color {Boolean} (red|blue)', () => {
      expect(option("+ -c -r --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('+ -c -r --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("+ -c -r --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": false,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    // [object Object]
    test('!+', () => {
      expect(() => option("!+"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ [red]', () => {
      expect(() => option("!+ [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ (red|blue)', () => {
      expect(() => option("!+ (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ (red|blue) [red]', () => {
      expect(() => option("!+ (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {String}', () => {
      expect(() => option("!+ {String}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {String} [red]', () => {
      expect(() => option("!+ {String} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {String} (red|blue)', () => {
      expect(() => option("!+ {String} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {String} (red|blue) [red]', () => {
      expect(() => option("!+ {String} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Integer}', () => {
      expect(() => option("!+ {Integer}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Integer} [red]', () => {
      expect(() => option("!+ {Integer} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Integer} (red|blue)', () => {
      expect(() => option("!+ {Integer} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Integer} (red|blue) [red]', () => {
      expect(() => option("!+ {Integer} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Number}', () => {
      expect(() => option("!+ {Number}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Number} [red]', () => {
      expect(() => option("!+ {Number} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Number} (red|blue)', () => {
      expect(() => option("!+ {Number} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Number} (red|blue) [red]', () => {
      expect(() => option("!+ {Number} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Boolean}', () => {
      expect(() => option("!+ {Boolean}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Boolean} [red]', () => {
      expect(() => option("!+ {Boolean} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Boolean} (red|blue)', () => {
      expect(() => option("!+ {Boolean} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('!+ {Boolean} (red|blue) [red]', () => {
      expect(() => option("!+ {Boolean} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    test('!+ --color', () => {
      expect(option("!+ --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ]
        });
    });
  

    test('!+ --color [red]', () => {
      expect(option("!+ --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color (red|blue)', () => {
      expect(option("!+ --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color (red|blue) [red]', () => {
      expect(option("!+ --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {String}', () => {
      expect(option("!+ --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ --color {String} [red]', () => {
      expect(option("!+ --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {String} (red|blue)', () => {
      expect(option("!+ --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color {String} (red|blue) [red]', () => {
      expect(option("!+ --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {Integer}', () => {
      expect(option("!+ --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ --color {Integer} [red]', () => {
      expect(option("!+ --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {Integer} (red|blue)', () => {
      expect(option("!+ --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color {Integer} (red|blue) [red]', () => {
      expect(option("!+ --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {Number}', () => {
      expect(option("!+ --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ --color {Number} [red]', () => {
      expect(option("!+ --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {Number} (red|blue)', () => {
      expect(option("!+ --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color {Number} (red|blue) [red]', () => {
      expect(option("!+ --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {Boolean}', () => {
      expect(option("!+ --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ --color {Boolean} [red]', () => {
      expect(option("!+ --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color {Boolean} (red|blue)', () => {
      expect(option("!+ --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color {Boolean} (red|blue) [red]', () => {
      expect(option("!+ --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color', () => {
      expect(option("!+ --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ]
        });
    });
  

    test('!+ --color --set-color [red]', () => {
      expect(option("!+ --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color (red|blue)', () => {
      expect(option("!+ --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color --set-color (red|blue) [red]', () => {
      expect(option("!+ --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {String}', () => {
      expect(option("!+ --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ --color --set-color {String} [red]', () => {
      expect(option("!+ --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {String} (red|blue)', () => {
      expect(option("!+ --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color --set-color {String} (red|blue) [red]', () => {
      expect(option("!+ --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {Integer}', () => {
      expect(option("!+ --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ --color --set-color {Integer} [red]', () => {
      expect(option("!+ --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {Integer} (red|blue)', () => {
      expect(option("!+ --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("!+ --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {Number}', () => {
      expect(option("!+ --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ --color --set-color {Number} [red]', () => {
      expect(option("!+ --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {Number} (red|blue)', () => {
      expect(option("!+ --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("!+ --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {Boolean}', () => {
      expect(option("!+ --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ --color --set-color {Boolean} [red]', () => {
      expect(option("!+ --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ --color --set-color {Boolean} (red|blue)', () => {
      expect(option("!+ --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("!+ --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c', () => {
      expect(option("!+ -c").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ]
        });
    });
  

    test('!+ -c [red]', () => {
      expect(option("!+ -c [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c (red|blue)', () => {
      expect(option("!+ -c (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c (red|blue) [red]', () => {
      expect(option("!+ -c (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {String}', () => {
      expect(option("!+ -c {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ -c {String} [red]', () => {
      expect(option("!+ -c {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {String} (red|blue)', () => {
      expect(option("!+ -c {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c {String} (red|blue) [red]', () => {
      expect(option("!+ -c {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {Integer}', () => {
      expect(option("!+ -c {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ -c {Integer} [red]', () => {
      expect(option("!+ -c {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {Integer} (red|blue)', () => {
      expect(option("!+ -c {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c {Integer} (red|blue) [red]', () => {
      expect(option("!+ -c {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {Number}', () => {
      expect(option("!+ -c {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ -c {Number} [red]', () => {
      expect(option("!+ -c {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {Number} (red|blue)', () => {
      expect(option("!+ -c {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c {Number} (red|blue) [red]', () => {
      expect(option("!+ -c {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {Boolean}', () => {
      expect(option("!+ -c {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ -c {Boolean} [red]', () => {
      expect(option("!+ -c {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c {Boolean} (red|blue)', () => {
      expect(option("!+ -c {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c {Boolean} (red|blue) [red]', () => {
      expect(option("!+ -c {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color', () => {
      expect(option("!+ -c --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ]
        });
    });
  

    test('!+ -c --color [red]', () => {
      expect(option("!+ -c --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color (red|blue)', () => {
      expect(option("!+ -c --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color (red|blue) [red]', () => {
      expect(option("!+ -c --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {String}', () => {
      expect(option("!+ -c --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ -c --color {String} [red]', () => {
      expect(option("!+ -c --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {String} (red|blue)', () => {
      expect(option("!+ -c --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color {String} (red|blue) [red]', () => {
      expect(option("!+ -c --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {Integer}', () => {
      expect(option("!+ -c --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ -c --color {Integer} [red]', () => {
      expect(option("!+ -c --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {Integer} (red|blue)', () => {
      expect(option("!+ -c --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color {Integer} (red|blue) [red]', () => {
      expect(option("!+ -c --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {Number}', () => {
      expect(option("!+ -c --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ -c --color {Number} [red]', () => {
      expect(option("!+ -c --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {Number} (red|blue)', () => {
      expect(option("!+ -c --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color {Number} (red|blue) [red]', () => {
      expect(option("!+ -c --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {Boolean}', () => {
      expect(option("!+ -c --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ -c --color {Boolean} [red]', () => {
      expect(option("!+ -c --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color {Boolean} (red|blue)', () => {
      expect(option("!+ -c --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color {Boolean} (red|blue) [red]', () => {
      expect(option("!+ -c --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color', () => {
      expect(option("!+ -c --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ]
        });
    });
  

    test('!+ -c --color --set-color [red]', () => {
      expect(option("!+ -c --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color (red|blue)', () => {
      expect(option("!+ -c --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color --set-color (red|blue) [red]', () => {
      expect(option("!+ -c --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {String}', () => {
      expect(option("!+ -c --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ -c --color --set-color {String} [red]', () => {
      expect(option("!+ -c --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {String} (red|blue)', () => {
      expect(option("!+ -c --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color --set-color {String} (red|blue) [red]', () => {
      expect(option("!+ -c --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {Integer}', () => {
      expect(option("!+ -c --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ -c --color --set-color {Integer} [red]', () => {
      expect(option("!+ -c --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {Integer} (red|blue)', () => {
      expect(option("!+ -c --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("!+ -c --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {Number}', () => {
      expect(option("!+ -c --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ -c --color --set-color {Number} [red]', () => {
      expect(option("!+ -c --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {Number} (red|blue)', () => {
      expect(option("!+ -c --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("!+ -c --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {Boolean}', () => {
      expect(option("!+ -c --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ -c --color --set-color {Boolean} [red]', () => {
      expect(option("!+ -c --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c --color --set-color {Boolean} (red|blue)', () => {
      expect(option("!+ -c --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("!+ -c --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r', () => {
      expect(option("!+ -c -r").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ]
        });
    });
  

    test('!+ -c -r [red]', () => {
      expect(option("!+ -c -r [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r (red|blue)', () => {
      expect(option("!+ -c -r (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r (red|blue) [red]', () => {
      expect(option("!+ -c -r (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {String}', () => {
      expect(option("!+ -c -r {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ -c -r {String} [red]', () => {
      expect(option("!+ -c -r {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {String} (red|blue)', () => {
      expect(option("!+ -c -r {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r {String} (red|blue) [red]', () => {
      expect(option("!+ -c -r {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {Integer}', () => {
      expect(option("!+ -c -r {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ -c -r {Integer} [red]', () => {
      expect(option("!+ -c -r {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {Integer} (red|blue)', () => {
      expect(option("!+ -c -r {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r {Integer} (red|blue) [red]', () => {
      expect(option("!+ -c -r {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {Number}', () => {
      expect(option("!+ -c -r {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ -c -r {Number} [red]', () => {
      expect(option("!+ -c -r {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {Number} (red|blue)', () => {
      expect(option("!+ -c -r {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r {Number} (red|blue) [red]', () => {
      expect(option("!+ -c -r {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {Boolean}', () => {
      expect(option("!+ -c -r {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ -c -r {Boolean} [red]', () => {
      expect(option("!+ -c -r {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r {Boolean} (red|blue)', () => {
      expect(option("!+ -c -r {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r {Boolean} (red|blue) [red]', () => {
      expect(option("!+ -c -r {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color', () => {
      expect(option("!+ -c -r --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ]
        });
    });
  

    test('!+ -c -r --color [red]', () => {
      expect(option("!+ -c -r --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color (red|blue)', () => {
      expect(option("!+ -c -r --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color (red|blue) [red]', () => {
      expect(option("!+ -c -r --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {String}', () => {
      expect(option("!+ -c -r --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ -c -r --color {String} [red]', () => {
      expect(option("!+ -c -r --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {String} (red|blue)', () => {
      expect(option("!+ -c -r --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color {String} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {Integer}', () => {
      expect(option("!+ -c -r --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ -c -r --color {Integer} [red]', () => {
      expect(option("!+ -c -r --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {Integer} (red|blue)', () => {
      expect(option("!+ -c -r --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color {Integer} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {Number}', () => {
      expect(option("!+ -c -r --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ -c -r --color {Number} [red]', () => {
      expect(option("!+ -c -r --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {Number} (red|blue)', () => {
      expect(option("!+ -c -r --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color {Number} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {Boolean}', () => {
      expect(option("!+ -c -r --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ -c -r --color {Boolean} [red]', () => {
      expect(option("!+ -c -r --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color {Boolean} (red|blue)', () => {
      expect(option("!+ -c -r --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color {Boolean} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color', () => {
      expect(option("!+ -c -r --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color [red]', () => {
      expect(option("!+ -c -r --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color (red|blue)', () => {
      expect(option("!+ -c -r --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color (red|blue) [red]', () => {
      expect(option("!+ -c -r --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {String}', () => {
      expect(option("!+ -c -r --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {String} [red]', () => {
      expect(option("!+ -c -r --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {String} (red|blue)', () => {
      expect(option("!+ -c -r --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {String} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {Integer}', () => {
      expect(option("!+ -c -r --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {Integer} [red]', () => {
      expect(option("!+ -c -r --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {Integer} (red|blue)', () => {
      expect(option("!+ -c -r --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {Number}', () => {
      expect(option("!+ -c -r --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {Number} [red]', () => {
      expect(option("!+ -c -r --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {Number} (red|blue)', () => {
      expect(option("!+ -c -r --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {Boolean}', () => {
      expect(option("!+ -c -r --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {Boolean} [red]', () => {
      expect(option("!+ -c -r --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('!+ -c -r --color --set-color {Boolean} (red|blue)', () => {
      expect(option("!+ -c -r --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('!+ -c -r --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("!+ -c -r --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": false,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    // [object Object]
    test('@!?', () => {
      expect(() => option("@!?"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? [red]', () => {
      expect(() => option("@!? [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? (red|blue)', () => {
      expect(() => option("@!? (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? (red|blue) [red]', () => {
      expect(() => option("@!? (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {String}', () => {
      expect(() => option("@!? {String}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {String} [red]', () => {
      expect(() => option("@!? {String} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {String} (red|blue)', () => {
      expect(() => option("@!? {String} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {String} (red|blue) [red]', () => {
      expect(() => option("@!? {String} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Integer}', () => {
      expect(() => option("@!? {Integer}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Integer} [red]', () => {
      expect(() => option("@!? {Integer} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Integer} (red|blue)', () => {
      expect(() => option("@!? {Integer} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Integer} (red|blue) [red]', () => {
      expect(() => option("@!? {Integer} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Number}', () => {
      expect(() => option("@!? {Number}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Number} [red]', () => {
      expect(() => option("@!? {Number} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Number} (red|blue)', () => {
      expect(() => option("@!? {Number} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Number} (red|blue) [red]', () => {
      expect(() => option("@!? {Number} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Boolean}', () => {
      expect(() => option("@!? {Boolean}"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Boolean} [red]', () => {
      expect(() => option("@!? {Boolean} [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Boolean} (red|blue)', () => {
      expect(() => option("@!? {Boolean} (red|blue)"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    // [object Object]
    test('@!? {Boolean} (red|blue) [red]', () => {
      expect(() => option("@!? {Boolean} (red|blue) [red]"))
        .toThrow("Option tokens cannot be an empty array.");
    });
    

    test('@!? --color', () => {
      expect(option("@!? --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ]
        });
    });
  

    test('@!? --color [red]', () => {
      expect(option("@!? --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color (red|blue)', () => {
      expect(option("@!? --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color (red|blue) [red]', () => {
      expect(option("@!? --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {String}', () => {
      expect(option("@!? --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? --color {String} [red]', () => {
      expect(option("@!? --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {String} (red|blue)', () => {
      expect(option("@!? --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color {String} (red|blue) [red]', () => {
      expect(option("@!? --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {Integer}', () => {
      expect(option("@!? --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? --color {Integer} [red]', () => {
      expect(option("@!? --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {Integer} (red|blue)', () => {
      expect(option("@!? --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color {Integer} (red|blue) [red]', () => {
      expect(option("@!? --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {Number}', () => {
      expect(option("@!? --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? --color {Number} [red]', () => {
      expect(option("@!? --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {Number} (red|blue)', () => {
      expect(option("@!? --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color {Number} (red|blue) [red]', () => {
      expect(option("@!? --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {Boolean}', () => {
      expect(option("@!? --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? --color {Boolean} [red]', () => {
      expect(option("@!? --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color {Boolean} (red|blue)', () => {
      expect(option("@!? --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color {Boolean} (red|blue) [red]', () => {
      expect(option("@!? --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color', () => {
      expect(option("@!? --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ]
        });
    });
  

    test('@!? --color --set-color [red]', () => {
      expect(option("@!? --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color (red|blue)', () => {
      expect(option("@!? --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color --set-color (red|blue) [red]', () => {
      expect(option("@!? --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {String}', () => {
      expect(option("@!? --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? --color --set-color {String} [red]', () => {
      expect(option("@!? --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {String} (red|blue)', () => {
      expect(option("@!? --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color --set-color {String} (red|blue) [red]', () => {
      expect(option("@!? --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {Integer}', () => {
      expect(option("@!? --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? --color --set-color {Integer} [red]', () => {
      expect(option("@!? --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {Integer} (red|blue)', () => {
      expect(option("@!? --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("@!? --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {Number}', () => {
      expect(option("@!? --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? --color --set-color {Number} [red]', () => {
      expect(option("@!? --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {Number} (red|blue)', () => {
      expect(option("@!? --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("@!? --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {Boolean}', () => {
      expect(option("@!? --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? --color --set-color {Boolean} [red]', () => {
      expect(option("@!? --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? --color --set-color {Boolean} (red|blue)', () => {
      expect(option("@!? --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("@!? --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c', () => {
      expect(option("@!? -c").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ]
        });
    });
  

    test('@!? -c [red]', () => {
      expect(option("@!? -c [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c (red|blue)', () => {
      expect(option("@!? -c (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c (red|blue) [red]', () => {
      expect(option("@!? -c (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {String}', () => {
      expect(option("@!? -c {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? -c {String} [red]', () => {
      expect(option("@!? -c {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {String} (red|blue)', () => {
      expect(option("@!? -c {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c {String} (red|blue) [red]', () => {
      expect(option("@!? -c {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {Integer}', () => {
      expect(option("@!? -c {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? -c {Integer} [red]', () => {
      expect(option("@!? -c {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {Integer} (red|blue)', () => {
      expect(option("@!? -c {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c {Integer} (red|blue) [red]', () => {
      expect(option("@!? -c {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {Number}', () => {
      expect(option("@!? -c {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? -c {Number} [red]', () => {
      expect(option("@!? -c {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {Number} (red|blue)', () => {
      expect(option("@!? -c {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c {Number} (red|blue) [red]', () => {
      expect(option("@!? -c {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {Boolean}', () => {
      expect(option("@!? -c {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? -c {Boolean} [red]', () => {
      expect(option("@!? -c {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c {Boolean} (red|blue)', () => {
      expect(option("@!? -c {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c {Boolean} (red|blue) [red]', () => {
      expect(option("@!? -c {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color', () => {
      expect(option("@!? -c --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ]
        });
    });
  

    test('@!? -c --color [red]', () => {
      expect(option("@!? -c --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color (red|blue)', () => {
      expect(option("@!? -c --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color (red|blue) [red]', () => {
      expect(option("@!? -c --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {String}', () => {
      expect(option("@!? -c --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? -c --color {String} [red]', () => {
      expect(option("@!? -c --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {String} (red|blue)', () => {
      expect(option("@!? -c --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color {String} (red|blue) [red]', () => {
      expect(option("@!? -c --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {Integer}', () => {
      expect(option("@!? -c --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? -c --color {Integer} [red]', () => {
      expect(option("@!? -c --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {Integer} (red|blue)', () => {
      expect(option("@!? -c --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color {Integer} (red|blue) [red]', () => {
      expect(option("@!? -c --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {Number}', () => {
      expect(option("@!? -c --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? -c --color {Number} [red]', () => {
      expect(option("@!? -c --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {Number} (red|blue)', () => {
      expect(option("@!? -c --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color {Number} (red|blue) [red]', () => {
      expect(option("@!? -c --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {Boolean}', () => {
      expect(option("@!? -c --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? -c --color {Boolean} [red]', () => {
      expect(option("@!? -c --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color {Boolean} (red|blue)', () => {
      expect(option("@!? -c --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color {Boolean} (red|blue) [red]', () => {
      expect(option("@!? -c --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color', () => {
      expect(option("@!? -c --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ]
        });
    });
  

    test('@!? -c --color --set-color [red]', () => {
      expect(option("@!? -c --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color (red|blue)', () => {
      expect(option("@!? -c --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color --set-color (red|blue) [red]', () => {
      expect(option("@!? -c --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {String}', () => {
      expect(option("@!? -c --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? -c --color --set-color {String} [red]', () => {
      expect(option("@!? -c --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {String} (red|blue)', () => {
      expect(option("@!? -c --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color --set-color {String} (red|blue) [red]', () => {
      expect(option("@!? -c --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {Integer}', () => {
      expect(option("@!? -c --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? -c --color --set-color {Integer} [red]', () => {
      expect(option("@!? -c --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {Integer} (red|blue)', () => {
      expect(option("@!? -c --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("@!? -c --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {Number}', () => {
      expect(option("@!? -c --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? -c --color --set-color {Number} [red]', () => {
      expect(option("@!? -c --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {Number} (red|blue)', () => {
      expect(option("@!? -c --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("@!? -c --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {Boolean}', () => {
      expect(option("@!? -c --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? -c --color --set-color {Boolean} [red]', () => {
      expect(option("@!? -c --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c --color --set-color {Boolean} (red|blue)', () => {
      expect(option("@!? -c --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("@!? -c --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r', () => {
      expect(option("@!? -c -r").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ]
        });
    });
  

    test('@!? -c -r [red]', () => {
      expect(option("@!? -c -r [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r (red|blue)', () => {
      expect(option("@!? -c -r (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r (red|blue) [red]', () => {
      expect(option("@!? -c -r (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {String}', () => {
      expect(option("@!? -c -r {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? -c -r {String} [red]', () => {
      expect(option("@!? -c -r {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {String} (red|blue)', () => {
      expect(option("@!? -c -r {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r {String} (red|blue) [red]', () => {
      expect(option("@!? -c -r {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {Integer}', () => {
      expect(option("@!? -c -r {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? -c -r {Integer} [red]', () => {
      expect(option("@!? -c -r {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {Integer} (red|blue)', () => {
      expect(option("@!? -c -r {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r {Integer} (red|blue) [red]', () => {
      expect(option("@!? -c -r {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {Number}', () => {
      expect(option("@!? -c -r {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? -c -r {Number} [red]', () => {
      expect(option("@!? -c -r {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {Number} (red|blue)', () => {
      expect(option("@!? -c -r {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r {Number} (red|blue) [red]', () => {
      expect(option("@!? -c -r {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {Boolean}', () => {
      expect(option("@!? -c -r {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? -c -r {Boolean} [red]', () => {
      expect(option("@!? -c -r {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r {Boolean} (red|blue)', () => {
      expect(option("@!? -c -r {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r {Boolean} (red|blue) [red]', () => {
      expect(option("@!? -c -r {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color', () => {
      expect(option("@!? -c -r --color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ]
        });
    });
  

    test('@!? -c -r --color [red]', () => {
      expect(option("@!? -c -r --color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color (red|blue)', () => {
      expect(option("@!? -c -r --color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color (red|blue) [red]', () => {
      expect(option("@!? -c -r --color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {String}', () => {
      expect(option("@!? -c -r --color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? -c -r --color {String} [red]', () => {
      expect(option("@!? -c -r --color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {String} (red|blue)', () => {
      expect(option("@!? -c -r --color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color {String} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {Integer}', () => {
      expect(option("@!? -c -r --color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? -c -r --color {Integer} [red]', () => {
      expect(option("@!? -c -r --color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {Integer} (red|blue)', () => {
      expect(option("@!? -c -r --color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color {Integer} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {Number}', () => {
      expect(option("@!? -c -r --color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? -c -r --color {Number} [red]', () => {
      expect(option("@!? -c -r --color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {Number} (red|blue)', () => {
      expect(option("@!? -c -r --color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color {Number} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {Boolean}', () => {
      expect(option("@!? -c -r --color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? -c -r --color {Boolean} [red]', () => {
      expect(option("@!? -c -r --color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color {Boolean} (red|blue)', () => {
      expect(option("@!? -c -r --color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color {Boolean} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color', () => {
      expect(option("@!? -c -r --color --set-color").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color [red]', () => {
      expect(option("@!? -c -r --color --set-color [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color (red|blue)', () => {
      expect(option("@!? -c -r --color --set-color (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color (red|blue) [red]', () => {
      expect(option("@!? -c -r --color --set-color (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {String}', () => {
      expect(option("@!? -c -r --color --set-color {String}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {String} [red]', () => {
      expect(option("@!? -c -r --color --set-color {String} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {String} (red|blue)', () => {
      expect(option("@!? -c -r --color --set-color {String} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {String} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color --set-color {String} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "String"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {Integer}', () => {
      expect(option("@!? -c -r --color --set-color {Integer}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {Integer} [red]', () => {
      expect(option("@!? -c -r --color --set-color {Integer} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {Integer} (red|blue)', () => {
      expect(option("@!? -c -r --color --set-color {Integer} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {Integer} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color --set-color {Integer} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Integer"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {Number}', () => {
      expect(option("@!? -c -r --color --set-color {Number}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {Number} [red]', () => {
      expect(option("@!? -c -r --color --set-color {Number} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {Number} (red|blue)', () => {
      expect(option("@!? -c -r --color --set-color {Number} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {Number} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color --set-color {Number} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Number"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {Boolean}', () => {
      expect(option("@!? -c -r --color --set-color {Boolean}").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {Boolean} [red]', () => {
      expect(option("@!? -c -r --color --set-color {Boolean} [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "default": "red"
        });
    });
  

    test('@!? -c -r --color --set-color {Boolean} (red|blue)', () => {
      expect(option("@!? -c -r --color --set-color {Boolean} (red|blue)").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ]
        });
    });
  

    test('@!? -c -r --color --set-color {Boolean} (red|blue) [red]', () => {
      expect(option("@!? -c -r --color --set-color {Boolean} (red|blue) [red]").spec)
        .toMatchObject({
          "isRequired": true,
          "allowMultiple": true,
          "tokens": [
            "c",
            "r",
            "color",
            "set-color"
          ],
          "types": [
            "Boolean"
          ],
          "values": [
            "red",
            "blue"
          ],
          "default": "red"
        });
    });
  
  });
});

/**
 * Each entry in this array is a single test.  Each test up to three
 * fields:
 *  - a test description
 *  - The resultant string version of the option params
 *  - an input to the option() function
 *  - an expected output object
 *
 *  If the expected output is omitted, then the input is also used as
 *  the output.  This helps with test brevity, and can be done because
 *  we expect the option() function to return a sanitized list of input
 *  parameters.
 *
 *  It is done this way to make it very clear what is being tested, in the
 *  most direct, declarative way possible.
 */

/**
 * Sad path tests describe cases in which the program gets input it
 * doesn't like, but handles them gracefully.
 *
 * In HTTP terms, these are like 400-series statuses.
 */
describe("sad-path", () => {
  /**
   * A set of tests for when things don't go as expected.
   */
  describe("object-specified", () => {
    // Get a fresh version of the base parameters every time.
    function getBaseParams (overrides) {
      return Object.assign({
        allowMultiple : false,
        default       : "blue",
        description   : "A color",
        internal      : "chosenColor",
        isRequired    : false,
        tokens        : ['c', 'color'],
        types         : ['String'],
        values        : ['red','green','blue'],
      }, overrides);
    }

    test('bad type', () => {
      let specStr = "-c --color {NotGood} <chosenColor> (red|green|blue) [blue] A color";
      expect(() => option(specStr)).toThrow();
    });

    test('isRequired must be a boolean', () => {
      let params = getBaseParams({isRequired: []});
      expect(() => option(params)).toThrow("isRequired must be a boolean");
    });

    test('isDeprecated must be a boolean', () => {
      let params = getBaseParams({isDeprecated: []});
      expect(() => option(params)).toThrow("isDeprecated must be a boolean");
    });

    test('non-boolean allowMultiple', () => {
      let params = getBaseParams();
      params.allowMultiple = "true";
      expect(() => option(params)).toThrow();

      params.allowMultiple = 82;
      expect(() => option(params)).toThrow();
    });

    test('bad default values', () => {
      let params = getBaseParams({default: []});
      expect(() => option(params)).toThrow(
        "Option defaults must be of a valid type; received:  []"
      );
    });

    test('bad descriptions', () => {
      let params = getBaseParams({description: 4});
      expect(() => option(params)).toThrow(
        'Option descriptions must be strings:  4'
      );
    });

    /*
    test('invalid internal', () => {
      let params = getBaseParams({internal: 'one two'});
      expect(() => option(params)).toThrow(
        'Option internal names must be alphanumeric strings:  one two'
      );
    });
    */

    test('token formats', () => {
      let params = getBaseParams({tokens: ['apple', 44]});
      expect(() => option(params)).toThrow();

      // Must be defined
      params.tokens = undefined;
      expect(() => option(params)).toThrow();

      // Must be an array
      params.tokens = "porktato";
      expect(() => option(params)).toThrow();
      params.tokens = 55;
      expect(() => option(params)).toThrow();

      // Can't be empty
      params.tokens = [];
      expect(() => option(params)).toThrow();
    });

    test('non-boolean isRequired', () => {
      let params = getBaseParams();
      params.allowMultiple = "true";
      expect(() => option(params)).toThrow();

      params.allowMultiple = 82;
      expect(() => option(params)).toThrow();
    });

    test('bad tokens', () => {
      let params = getBaseParams({tokens:['c','count?']});
      expect(() => option(params))
        .toThrow("Long options cannot contain question marks:  count?");

      params = getBaseParams({tokens:['c','']});
      expect(() => option(params))
        .toThrow("Tokens cannot be empty strings.");

      params = getBaseParams({tokens:['c', 99]});
      expect(() => option(params))
        .toThrow("Tokens must be strings; received:  99");
    });

    test('bad type', () => {
      let params = getBaseParams({types: ['String','Vinegar']});
      expect(() => option(params)).toThrow(
        'Invalid option type specified:  Vinegar\nOption types must be one of:  String, Str, Number, Num, Integer, Int, Boolean, Bool',
      );

      params.types = "porktato";
      expect(() => option(params)).toThrow();

      params.types = [];
      expect(() => option(params)).toThrow();
    });

    describe('bad values', () => {
      // These values are just bad by themselves.
      [
        ["pif", "Option values must be an array."],
        [83,    "Option values must be an array."],
        [null,  "Option values must be an array."],
        [[],    "Option values cannot be an empty array."]
      ].forEach(([badValue,message]) => {
        test('' + JSON.stringify(badValue), () => {
          expect(() => option(getBaseParams({values:badValue})))
            .toThrow(message);
        });
      });
    });
  });
});
